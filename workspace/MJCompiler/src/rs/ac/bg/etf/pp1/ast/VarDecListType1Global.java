// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class VarDecListType1Global extends VarDecListTypeGlobal {

    private Type Type;
    private VarDeclListGlobal VarDeclListGlobal;

    public VarDecListType1Global (Type Type, VarDeclListGlobal VarDeclListGlobal) {
        this.Type=Type;
        if(Type!=null) Type.setParent(this);
        this.VarDeclListGlobal=VarDeclListGlobal;
        if(VarDeclListGlobal!=null) VarDeclListGlobal.setParent(this);
    }

    public Type getType() {
        return Type;
    }

    public void setType(Type Type) {
        this.Type=Type;
    }

    public VarDeclListGlobal getVarDeclListGlobal() {
        return VarDeclListGlobal;
    }

    public void setVarDeclListGlobal(VarDeclListGlobal VarDeclListGlobal) {
        this.VarDeclListGlobal=VarDeclListGlobal;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Type!=null) Type.accept(visitor);
        if(VarDeclListGlobal!=null) VarDeclListGlobal.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Type!=null) Type.traverseTopDown(visitor);
        if(VarDeclListGlobal!=null) VarDeclListGlobal.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Type!=null) Type.traverseBottomUp(visitor);
        if(VarDeclListGlobal!=null) VarDeclListGlobal.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("VarDecListType1Global(\n");

        if(Type!=null)
            buffer.append(Type.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarDeclListGlobal!=null)
            buffer.append(VarDeclListGlobal.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [VarDecListType1Global]");
        return buffer.toString();
    }
}
