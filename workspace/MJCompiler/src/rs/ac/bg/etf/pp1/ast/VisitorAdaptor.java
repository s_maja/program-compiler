// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public abstract class VisitorAdaptor implements Visitor { 

    public void visit(VarDeclGlobal VarDeclGlobal) { }
    public void visit(Unmatched Unmatched) { }
    public void visit(MethodDeclartions MethodDeclartions) { }
    public void visit(MethodDecl MethodDecl) { }
    public void visit(Mulop Mulop) { }
    public void visit(AbstractClassDeclList AbstractClassDeclList) { }
    public void visit(DesignatorOptions DesignatorOptions) { }
    public void visit(Matched Matched) { }
    public void visit(Relop Relop) { }
    public void visit(TermList TermList) { }
    public void visit(FactorExprOptional FactorExprOptional) { }
    public void visit(ConstDecListType ConstDecListType) { }
    public void visit(FormalParamDecl FormalParamDecl) { }
    public void visit(AbstractClassDeclListType AbstractClassDeclListType) { }
    public void visit(ProgramAlterantives ProgramAlterantives) { }
    public void visit(StatementList StatementList) { }
    public void visit(Addop Addop) { }
    public void visit(ClassDecListType ClassDecListType) { }
    public void visit(ForCondition ForCondition) { }
    public void visit(Factor Factor) { }
    public void visit(CondTerm CondTerm) { }
    public void visit(PrintOption PrintOption) { }
    public void visit(Condition Condition) { }
    public void visit(ConstDeclList ConstDeclList) { }
    public void visit(ActualParamList ActualParamList) { }
    public void visit(ExprList ExprList) { }
    public void visit(ReturnOption ReturnOption) { }
    public void visit(VarDeclarationsForClasses VarDeclarationsForClasses) { }
    public void visit(Declarations Declarations) { }
    public void visit(VarDecListType VarDecListType) { }
    public void visit(VarDeclList VarDeclList) { }
    public void visit(FormalParamList FormalParamList) { }
    public void visit(Expr Expr) { }
    public void visit(CondFactOption CondFactOption) { }
    public void visit(VarDecListTypeGlobal VarDecListTypeGlobal) { }
    public void visit(MethodTypeName MethodTypeName) { }
    public void visit(DesignatorStatement DesignatorStatement) { }
    public void visit(ActualPars ActualPars) { }
    public void visit(Statement Statement) { }
    public void visit(VarDecl VarDecl) { }
    public void visit(ForDesignatorStmt ForDesignatorStmt) { }
    public void visit(FactorDesignatorOptional FactorDesignatorOptional) { }
    public void visit(ConstDecl ConstDecl) { }
    public void visit(ClassDecList ClassDecList) { }
    public void visit(MethodDeclList MethodDeclList) { }
    public void visit(VarDeclListGlobal VarDeclListGlobal) { }
    public void visit(FormPars FormPars) { }
    public void visit(NewOp NewOp) { visit(); }
    public void visit(Modop Modop) { visit(); }
    public void visit(Divop Divop) { visit(); }
    public void visit(Mulope Mulope) { visit(); }
    public void visit(Minusop Minusop) { visit(); }
    public void visit(Addope Addope) { visit(); }
    public void visit(RelopGreaterEquals RelopGreaterEquals) { visit(); }
    public void visit(RelopGreater RelopGreater) { visit(); }
    public void visit(RelopLessEquals RelopLessEquals) { visit(); }
    public void visit(RelopLess RelopLess) { visit(); }
    public void visit(RelopNotEquals RelopNotEquals) { visit(); }
    public void visit(RelopEquals RelopEquals) { visit(); }
    public void visit(Assignop Assignop) { visit(); }
    public void visit(Dummy Dummy) { visit(); }
    public void visit(NoDesignatorOption NoDesignatorOption) { visit(); }
    public void visit(DesignatorOptionArray DesignatorOptionArray) { visit(); }
    public void visit(DesignatorOptionDot DesignatorOptionDot) { visit(); }
    public void visit(Designator Designator) { visit(); }
    public void visit(Var Var) { visit(); }
    public void visit(FuncCall FuncCall) { visit(); }
    public void visit(FactorDesignator FactorDesignator) { visit(); }
    public void visit(FactorExpr FactorExpr) { visit(); }
    public void visit(FactorNew FactorNew) { visit(); }
    public void visit(FactorBool FactorBool) { visit(); }
    public void visit(FactorChar FactorChar) { visit(); }
    public void visit(Const Const) { visit(); }
    public void visit(NoTermList NoTermList) { visit(); }
    public void visit(ListTerm ListTerm) { visit(); }
    public void visit(Term Term) { visit(); }
    public void visit(MinusTermExpr MinusTermExpr) { visit(); }
    public void visit(NoExprList NoExprList) { visit(); }
    public void visit(AddExpr AddExpr) { visit(); }
    public void visit(TermExpr TermExpr) { visit(); }
    public void visit(NoCondFactOption NoCondFactOption) { visit(); }
    public void visit(CondFactOptionClass CondFactOptionClass) { visit(); }
    public void visit(CondFact CondFact) { visit(); }
    public void visit(CondTermOne CondTermOne) { visit(); }
    public void visit(CondTermMore CondTermMore) { visit(); }
    public void visit(OrTerm OrTerm) { visit(); }
    public void visit(OneConditionTerm OneConditionTerm) { visit(); }
    public void visit(ConditionTerm ConditionTerm) { visit(); }
    public void visit(DummyJump DummyJump) { visit(); }
    public void visit(DummyForJmpInc DummyForJmpInc) { visit(); }
    public void visit(DummyForJmpCond DummyForJmpCond) { visit(); }
    public void visit(DummyForCondStart DummyForCondStart) { visit(); }
    public void visit(DummyStartIf DummyStartIf) { visit(); }
    public void visit(DummyEnd DummyEnd) { visit(); }
    public void visit(DummyIf DummyIf) { visit(); }
    public void visit(DummyElse DummyElse) { visit(); }
    public void visit(ConditionFixed ConditionFixed) { visit(); }
    public void visit(ActualParam ActualParam) { visit(); }
    public void visit(ActualParams ActualParams) { visit(); }
    public void visit(NoActuals NoActuals) { visit(); }
    public void visit(Actuals Actuals) { visit(); }
    public void visit(NewOpStatement NewOpStatement) { visit(); }
    public void visit(DesignatorStatementDec DesignatorStatementDec) { visit(); }
    public void visit(DesignatorStatementInc DesignatorStatementInc) { visit(); }
    public void visit(DesignatorStatementActParams DesignatorStatementActParams) { visit(); }
    public void visit(DesignatorStatementExpr DesignatorStatementExpr) { visit(); }
    public void visit(NoPrintOption NoPrintOption) { visit(); }
    public void visit(PrintOptionNum PrintOptionNum) { visit(); }
    public void visit(ReturnNoExpr ReturnNoExpr) { visit(); }
    public void visit(ReturnExpr ReturnExpr) { visit(); }
    public void visit(ConditionError ConditionError) { visit(); }
    public void visit(NoForCondition NoForCondition) { visit(); }
    public void visit(ForConditionClass ForConditionClass) { visit(); }
    public void visit(NoForDesignatorStmt NoForDesignatorStmt) { visit(); }
    public void visit(ForDesignatorStatement ForDesignatorStatement) { visit(); }
    public void visit(ForStart ForStart) { visit(); }
    public void visit(MatchedIf MatchedIf) { visit(); }
    public void visit(RecursionStatement RecursionStatement) { visit(); }
    public void visit(ReadStatement ReadStatement) { visit(); }
    public void visit(PrintStmt PrintStmt) { visit(); }
    public void visit(ReturnExprStmt ReturnExprStmt) { visit(); }
    public void visit(ContinueStatement ContinueStatement) { visit(); }
    public void visit(BreakStatement BreakStatement) { visit(); }
    public void visit(ForStatement ForStatement) { visit(); }
    public void visit(DesignatorStatementExprError DesignatorStatementExprError) { visit(); }
    public void visit(Assignment Assignment) { visit(); }
    public void visit(UnmatchedFor UnmatchedFor) { visit(); }
    public void visit(UnmatchedIfElse UnmatchedIfElse) { visit(); }
    public void visit(UnmatchedIf UnmatchedIf) { visit(); }
    public void visit(UnmachedStmt UnmachedStmt) { visit(); }
    public void visit(MatchedStmt MatchedStmt) { visit(); }
    public void visit(NoStmt NoStmt) { visit(); }
    public void visit(Statements Statements) { visit(); }
    public void visit(Type Type) { visit(); }
    public void visit(FormalParamDeclArray FormalParamDeclArray) { visit(); }
    public void visit(FormalParamDeclVar FormalParamDeclVar) { visit(); }
    public void visit(FormalParamDeclError FormalParamDeclError) { visit(); }
    public void visit(SingleFormalParamDecl SingleFormalParamDecl) { visit(); }
    public void visit(FormalParamDecls FormalParamDecls) { visit(); }
    public void visit(NoFormParam NoFormParam) { visit(); }
    public void visit(FormParams FormParams) { visit(); }
    public void visit(AbstractMethodDecl AbstractMethodDecl) { visit(); }
    public void visit(MethodTypeVoid MethodTypeVoid) { visit(); }
    public void visit(MethodTypeNameNonVoid MethodTypeNameNonVoid) { visit(); }
    public void visit(MethodDeclWithoutVar MethodDeclWithoutVar) { visit(); }
    public void visit(MethodDeclWithVar MethodDeclWithVar) { visit(); }
    public void visit(SingleMethodDeclList SingleMethodDeclList) { visit(); }
    public void visit(MethodDeclListClass MethodDeclListClass) { visit(); }
    public void visit(NoMethodDeclartions NoMethodDeclartions) { visit(); }
    public void visit(AbsMethodDeclartion AbsMethodDeclartion) { visit(); }
    public void visit(MethodDeclartion MethodDeclartion) { visit(); }
    public void visit(AbstractClassDeclListMethods AbstractClassDeclListMethods) { visit(); }
    public void visit(AbstractClassDeclListVarMethods AbstractClassDeclListVarMethods) { visit(); }
    public void visit(AbstractClassDeclListVar AbstractClassDeclListVar) { visit(); }
    public void visit(AbstractClassDeclListTypeExtends AbstractClassDeclListTypeExtends) { visit(); }
    public void visit(AbstractClassDeclListTypeClass AbstractClassDeclListTypeClass) { visit(); }
    public void visit(ClassDecListMethodsEmpty ClassDecListMethodsEmpty) { visit(); }
    public void visit(ClassDecListMethods ClassDecListMethods) { visit(); }
    public void visit(ClassDecListVarMethods ClassDecListVarMethods) { visit(); }
    public void visit(ClassDecListVar ClassDecListVar) { visit(); }
    public void visit(ClassDecListTypeExtends ClassDecListTypeExtends) { visit(); }
    public void visit(ClassDecListTypeClass ClassDecListTypeClass) { visit(); }
    public void visit(ConstDeclChar ConstDeclChar) { visit(); }
    public void visit(ConstDeclBool ConstDeclBool) { visit(); }
    public void visit(ConstDeclNumber ConstDeclNumber) { visit(); }
    public void visit(OneConstDecl OneConstDecl) { visit(); }
    public void visit(ConstDeclListMore ConstDeclListMore) { visit(); }
    public void visit(ConstDecListTypeClass ConstDecListTypeClass) { visit(); }
    public void visit(VarDeclSimple VarDeclSimple) { visit(); }
    public void visit(VarDeclArr VarDeclArr) { visit(); }
    public void visit(OneVarDecl OneVarDecl) { visit(); }
    public void visit(VarDeclListMore VarDeclListMore) { visit(); }
    public void visit(VarDecListTypeError VarDecListTypeError) { visit(); }
    public void visit(VarDecListType1 VarDecListType1) { visit(); }
    public void visit(VarDeclarationsOne VarDeclarationsOne) { visit(); }
    public void visit(VarDeclarationsClass VarDeclarationsClass) { visit(); }
    public void visit(VarDeclSimpleGlobal VarDeclSimpleGlobal) { visit(); }
    public void visit(VarDeclArrGlobal VarDeclArrGlobal) { visit(); }
    public void visit(OneVarDeclGlobal OneVarDeclGlobal) { visit(); }
    public void visit(VarDeclListMoreGlobal VarDeclListMoreGlobal) { visit(); }
    public void visit(VarDecListType1Global VarDecListType1Global) { visit(); }
    public void visit(NoDeclarations NoDeclarations) { visit(); }
    public void visit(AbstarctClassDeclarations AbstarctClassDeclarations) { visit(); }
    public void visit(ClassDeclarations ClassDeclarations) { visit(); }
    public void visit(ConstDeclarations ConstDeclarations) { visit(); }
    public void visit(VarDeclarations VarDeclarations) { visit(); }
    public void visit(ProgName ProgName) { visit(); }
    public void visit(ProgramNoMetod ProgramNoMetod) { visit(); }
    public void visit(ProgramClass ProgramClass) { visit(); }
    public void visit(Program Program) { visit(); }


    public void visit() { }
}
