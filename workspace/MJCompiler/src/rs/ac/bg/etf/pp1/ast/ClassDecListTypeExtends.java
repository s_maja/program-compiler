// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class ClassDecListTypeExtends extends ClassDecListType {

    private String className;
    private Type Type;
    private ClassDecList ClassDecList;

    public ClassDecListTypeExtends (String className, Type Type, ClassDecList ClassDecList) {
        this.className=className;
        this.Type=Type;
        if(Type!=null) Type.setParent(this);
        this.ClassDecList=ClassDecList;
        if(ClassDecList!=null) ClassDecList.setParent(this);
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className=className;
    }

    public Type getType() {
        return Type;
    }

    public void setType(Type Type) {
        this.Type=Type;
    }

    public ClassDecList getClassDecList() {
        return ClassDecList;
    }

    public void setClassDecList(ClassDecList ClassDecList) {
        this.ClassDecList=ClassDecList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Type!=null) Type.accept(visitor);
        if(ClassDecList!=null) ClassDecList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Type!=null) Type.traverseTopDown(visitor);
        if(ClassDecList!=null) ClassDecList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Type!=null) Type.traverseBottomUp(visitor);
        if(ClassDecList!=null) ClassDecList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ClassDecListTypeExtends(\n");

        buffer.append(" "+tab+className);
        buffer.append("\n");

        if(Type!=null)
            buffer.append(Type.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ClassDecList!=null)
            buffer.append(ClassDecList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ClassDecListTypeExtends]");
        return buffer.toString();
    }
}
