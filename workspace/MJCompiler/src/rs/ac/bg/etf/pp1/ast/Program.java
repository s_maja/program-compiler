// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:27


package rs.ac.bg.etf.pp1.ast;

public class Program implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    public rs.etf.pp1.symboltable.concepts.Obj obj = null;

    private ProgramAlterantives ProgramAlterantives;

    public Program (ProgramAlterantives ProgramAlterantives) {
        this.ProgramAlterantives=ProgramAlterantives;
        if(ProgramAlterantives!=null) ProgramAlterantives.setParent(this);
    }

    public ProgramAlterantives getProgramAlterantives() {
        return ProgramAlterantives;
    }

    public void setProgramAlterantives(ProgramAlterantives ProgramAlterantives) {
        this.ProgramAlterantives=ProgramAlterantives;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ProgramAlterantives!=null) ProgramAlterantives.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ProgramAlterantives!=null) ProgramAlterantives.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ProgramAlterantives!=null) ProgramAlterantives.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("Program(\n");

        if(ProgramAlterantives!=null)
            buffer.append(ProgramAlterantives.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [Program]");
        return buffer.toString();
    }
}
