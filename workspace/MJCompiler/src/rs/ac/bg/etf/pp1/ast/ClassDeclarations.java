// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:27


package rs.ac.bg.etf.pp1.ast;

public class ClassDeclarations extends Declarations {

    private Declarations Declarations;
    private ClassDecListType ClassDecListType;

    public ClassDeclarations (Declarations Declarations, ClassDecListType ClassDecListType) {
        this.Declarations=Declarations;
        if(Declarations!=null) Declarations.setParent(this);
        this.ClassDecListType=ClassDecListType;
        if(ClassDecListType!=null) ClassDecListType.setParent(this);
    }

    public Declarations getDeclarations() {
        return Declarations;
    }

    public void setDeclarations(Declarations Declarations) {
        this.Declarations=Declarations;
    }

    public ClassDecListType getClassDecListType() {
        return ClassDecListType;
    }

    public void setClassDecListType(ClassDecListType ClassDecListType) {
        this.ClassDecListType=ClassDecListType;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Declarations!=null) Declarations.accept(visitor);
        if(ClassDecListType!=null) ClassDecListType.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Declarations!=null) Declarations.traverseTopDown(visitor);
        if(ClassDecListType!=null) ClassDecListType.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Declarations!=null) Declarations.traverseBottomUp(visitor);
        if(ClassDecListType!=null) ClassDecListType.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ClassDeclarations(\n");

        if(Declarations!=null)
            buffer.append(Declarations.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ClassDecListType!=null)
            buffer.append(ClassDecListType.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ClassDeclarations]");
        return buffer.toString();
    }
}
