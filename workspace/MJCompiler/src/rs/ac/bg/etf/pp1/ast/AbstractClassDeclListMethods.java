// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class AbstractClassDeclListMethods extends AbstractClassDeclList {

    private MethodDeclartions MethodDeclartions;

    public AbstractClassDeclListMethods (MethodDeclartions MethodDeclartions) {
        this.MethodDeclartions=MethodDeclartions;
        if(MethodDeclartions!=null) MethodDeclartions.setParent(this);
    }

    public MethodDeclartions getMethodDeclartions() {
        return MethodDeclartions;
    }

    public void setMethodDeclartions(MethodDeclartions MethodDeclartions) {
        this.MethodDeclartions=MethodDeclartions;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(MethodDeclartions!=null) MethodDeclartions.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(MethodDeclartions!=null) MethodDeclartions.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(MethodDeclartions!=null) MethodDeclartions.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractClassDeclListMethods(\n");

        if(MethodDeclartions!=null)
            buffer.append(MethodDeclartions.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractClassDeclListMethods]");
        return buffer.toString();
    }
}
