// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class AbstractClassDeclListVarMethods extends AbstractClassDeclList {

    private VarDeclarationsForClasses VarDeclarationsForClasses;
    private MethodDeclartions MethodDeclartions;

    public AbstractClassDeclListVarMethods (VarDeclarationsForClasses VarDeclarationsForClasses, MethodDeclartions MethodDeclartions) {
        this.VarDeclarationsForClasses=VarDeclarationsForClasses;
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.setParent(this);
        this.MethodDeclartions=MethodDeclartions;
        if(MethodDeclartions!=null) MethodDeclartions.setParent(this);
    }

    public VarDeclarationsForClasses getVarDeclarationsForClasses() {
        return VarDeclarationsForClasses;
    }

    public void setVarDeclarationsForClasses(VarDeclarationsForClasses VarDeclarationsForClasses) {
        this.VarDeclarationsForClasses=VarDeclarationsForClasses;
    }

    public MethodDeclartions getMethodDeclartions() {
        return MethodDeclartions;
    }

    public void setMethodDeclartions(MethodDeclartions MethodDeclartions) {
        this.MethodDeclartions=MethodDeclartions;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.accept(visitor);
        if(MethodDeclartions!=null) MethodDeclartions.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.traverseTopDown(visitor);
        if(MethodDeclartions!=null) MethodDeclartions.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.traverseBottomUp(visitor);
        if(MethodDeclartions!=null) MethodDeclartions.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractClassDeclListVarMethods(\n");

        if(VarDeclarationsForClasses!=null)
            buffer.append(VarDeclarationsForClasses.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodDeclartions!=null)
            buffer.append(MethodDeclartions.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractClassDeclListVarMethods]");
        return buffer.toString();
    }
}
