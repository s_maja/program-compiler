package rs.ac.bg.etf.pp1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class SemanticAnalyzer extends VisitorAdaptor {

	int printCallCount = 0;
	int varDeclCount = 0;
	Obj currentMethod = null;
	boolean returnFound = false;
	boolean errorDetected = false;
	int nVars;
	String currType = "";
	boolean hasMain = false;
	int forDepth = 0;
	List<Struct> actParams = new ArrayList<>();

	
	Logger log = Logger.getLogger(getClass());

	public void report_error(String message, SyntaxNode info) {
		errorDetected = true;
		StringBuilder msg = new StringBuilder(message);
		int line = (info == null) ? 0: info.getLine();
		if (line != 0)
			msg.append (" na liniji ").append(line);
		log.error(msg.toString());
	}

	public void report_info(String message, SyntaxNode info) {
		StringBuilder msg = new StringBuilder(message); 
		int line = (info == null) ? 0: info.getLine();
		if (line != 0)
			msg.append (" na liniji ").append(line);
		log.info(msg.toString());
	}

	
	private String getKind(int i) {
		switch (i) {
		case 0:
			return "Con";
		case 1:
			return "Var";
		case 2:
			return "Type";
		case 3:
			return "Meth";
		case 4:
			return "Fld";
		case 5:
			return "Elem";
		case 6:
			return "Prog";
		}
		return "";
	}
	
	private String getType(int i) {
		switch (i) {
		case 0:
			return "none";
		case 1:
			return "int";
		case 2:
			return "char";
		case 3:
			return "Array";
		case 4:
			return "Class";
		case 5:
			return "bool";
		}
		return "";
	}

    public void visit(ProgName progName){
    	progName.obj = Tab.insert(Obj.Prog, progName.getPName(), Tab.noType);
    	Tab.openScope();
    }
    
    public void visit(ProgramClass program){
    	nVars = Tab.currentScope.getnVars();
    	Tab.chainLocalSymbols(program.getProgName().obj);
    	Tab.closeScope();
    }
    
    public void visit(ProgramNoMetod program){
    	nVars = Tab.currentScope.getnVars();
    	Tab.chainLocalSymbols(program.getProgName().obj);
    	Tab.closeScope();
    }
    
    public void visit(Type type){
    	Obj typeNode = Tab.find(type.getTypeName());
    	if(typeNode == Tab.noObj){
    		report_error("Nije pronadjen tip " + type.getTypeName() + " u tabeli simbola! ", null);
    		type.struct = Tab.noType;
    		currType = "";
    	}else{
    		if(Obj.Type == typeNode.getKind()){
    			type.struct = typeNode.getType();
    			currType = type.getTypeName();
    		}else{
    			report_error("Greska: Ime " + type.getTypeName() + " ne predstavlja tip!", type);
    			type.struct = Tab.noType;
    			currType = "";
    		}
    	}
    }
    
    public void visit(ConstDeclNumber constDeclNumber) {
    	if(!Tab.noObj.equals(Tab.find(constDeclNumber.getConstName()))){
    		report_error("Ime "+ constDeclNumber.getConstName()+" vec postoji u tabeli simbola", constDeclNumber);
    	}
    	
    	if(!currType.equals("int"))
    		report_error("Pogresan tip konstante "+ constDeclNumber.getConstName(), constDeclNumber);
    	
    	Obj constNode = Tab.insert(Obj.Con, constDeclNumber.getConstName(),Tab.find(currType).getType());
    	constNode.setAdr(constDeclNumber.getN1());
    	report_info("Deklarisana konstanta -> "+ getKind(constNode.getKind()) +" " +constNode.getName() + ": "+ getType(constNode.getType().getKind())
    	+" "+ constNode.getAdr()+","+constNode.getLevel()+  " " , constDeclNumber);
    }
    
    public void visit(ConstDeclBool constDeclBool) {
    	if(!Tab.noObj.equals(Tab.find(constDeclBool.getConstName()))){
    		report_error("Ime "+ constDeclBool.getConstName()+" vec postoji u tabeli simbola", constDeclBool);
    	}
    	
    	if(!currType.equals("bool"))
    		report_error("Pogresan tip konstante "+ constDeclBool.getConstName(), constDeclBool);
    	
    	Obj constNode = Tab.insert(Obj.Con, constDeclBool.getConstName(),Tab.find(currType).getType());
    	constNode.setAdr(constDeclBool.getBl() ? 1 : 0);
    	report_info("Deklarisana konstanta -> "+ getKind(constNode.getKind()) +" " +constNode.getName() + ": "+ getType(constNode.getType().getKind())
    	+" "+ constNode.getAdr()+","+constNode.getLevel()+  " " , constDeclBool);
    }
    
    public void visit(ConstDeclChar constDeclChar) {
    	if(!Tab.noObj.equals(Tab.find(constDeclChar.getConstName()))){
    		report_error("Ime "+ constDeclChar.getConstName()+" vec postoji u tabeli simbola", constDeclChar);
    	}
    	
    	if(!currType.equals("char"))
    		report_error("Pogresan tip konstante "+ constDeclChar.getConstName(), constDeclChar);
    	
    	
    	Obj constNode = Tab.insert(Obj.Con, constDeclChar.getConstName(),Tab.find(currType).getType());
    	constNode.setAdr(constDeclChar.getC1());
    	report_info("Deklarisana konstanta -> "+ getKind(constNode.getKind()) +" " +constNode.getName() + ": "+ getType(constNode.getType().getKind())
    	+" "+ constNode.getAdr()+","+constNode.getLevel()+  " " , constDeclChar);
    }

    public void visit(Designator designator) {
    	Obj obj = Tab.find(designator.getName());
    	designator.obj = obj;
		if (obj == Tab.noObj) { 
			report_error("Greska na liniji " + designator.getLine()+ " : ime "+designator.getName()+" nije deklarisano! ", null);
			return;
		}
		
    	//provjera pristupa elementu niza
    	if(designator.getDesignatorOptions() instanceof DesignatorOptionArray) {
    		Obj node = Tab.find(designator.getName());
    		
    		if(node.getType().getKind() != Struct.Array)
    			report_error("Pristup elementu niza promjenjive koja nije tipa Array", designator);
    		
    		report_info("Pristup elementu niza -> "+ getKind(node.getKind()) +" " +node.getName() + ": "+ getType(node.getType().getKind())
        	+" "+ node.getAdr()+","+node.getLevel()+  " " , designator);
    	}
				
		//provjera da li je pristup const
		if(obj.getKind()==obj.Con) {
			report_info("Pristup konstanti -> "+ getKind(obj.getKind()) +" " +obj.getName() + ": "+ getType(obj.getType().getKind())
        	+" "+ obj.getAdr()+","+obj.getLevel()+  " " , designator);
		}
		
		//provjera da li je pristup globalnoj promjenjivoj
		if(obj.getLevel()==0 && obj.getKind()==obj.Var) {
			report_info("Pristup globalnoj promjenjivoj -> "+ getKind(obj.getKind()) +" " +obj.getName() + ": "+ getType(obj.getType().getKind())
        	+" "+ obj.getAdr()+","+obj.getLevel()+  " " , designator);
		}
		
		//provjera da li je pristup formalnom parametru metoda
		if(obj.getLevel()==1 && obj.getKind()==obj.Var) {
			int numFormParams= currentMethod.getLevel();
			if(obj.getAdr()< numFormParams)
			report_info("Pristup formalnom parametru metode "+ currentMethod.getName()+ " -> "+ getKind(obj.getKind()) +" " 
			+obj.getName() + ": "+ getType(obj.getType().getKind())
        	+" "+ obj.getAdr()+","+obj.getLevel()+  " " , designator);
		}
		
    }
    
    public void visit(VarDeclArrGlobal varDeclArrGlobal) {
    	if(!Tab.noObj.equals(Tab.find(varDeclArrGlobal.getVarName()))){
    		report_error("Ime "+ varDeclArrGlobal.getVarName()+" vec postoji u tabeli simbola", varDeclArrGlobal);
    	}
    	
    	Obj varNode = Tab.insert(Obj.Var, varDeclArrGlobal.getVarName(), new Struct(Struct.Array, Tab.find(currType).getType()));
    	report_info("Deklarisana globalna promjenjiva -> "+ getKind(varNode.getKind()) +" " +varNode.getName() + ": "+ getType(varNode.getType().getKind())
    	+" "+ varNode.getAdr()+","+varNode.getLevel()+  " " , varDeclArrGlobal);
    }
    
    public void visit(VarDeclSimpleGlobal varDeclSimpleGlobal) {
    	if(!Tab.noObj.equals(Tab.find(varDeclSimpleGlobal.getVarName()))){
    		report_error("Ime "+ varDeclSimpleGlobal.getVarName()+" vec postoji u tabeli simbola", varDeclSimpleGlobal);
    	}
    	Obj varNode = Tab.insert(Obj.Var, varDeclSimpleGlobal.getVarName(),Tab.find(currType).getType());
    	
    	report_info("Deklarisana globalna promjenjiva -> "+ getKind(varNode.getKind()) +" " +varNode.getName() + ": "+ getType(varNode.getType().getKind())
    	+" "+ varNode.getAdr()+","+varNode.getLevel()+  " " , varDeclSimpleGlobal);
    }
    
    public void visit(UnmatchedFor unmatchedFor) {
    	//report_info("Detektovana FOR petlja", unmatchedFor);
    	forDepth--;
    }
    
    public void visit(ForStatement forStatement) { 
    	//report_info("Detektovana FOR petlja", forStatement.getParent());
    	forDepth--;
    }
    
    public void visit(ForStart forStart) {
    	forDepth++;
    	report_info("Detektovana FOR petlja", forStart.getParent());
    }
    
    public void visit(FuncCall funcCall) {
    	//pozivi globalnih funckija
    	Obj func = ((FactorDesignator)funcCall.getParent()).getDesignator().obj;
    	if(Obj.Meth == func.getKind()){
    		report_info("Poziv globalne funkcije -> "+ getKind(func.getKind()) +" " +func.getName() + ": "+ getType(func.getType().getKind())
        	+" "+ func.getAdr()+","+func.getLevel()+  " " , funcCall.getParent());
    		
    		
    		
    	}else{
			report_error("Greska na liniji " + funcCall.getParent().getLine()+"  nije funkcija!", null);
    	}
    }
    
    public void visit(DesignatorStatementActParams funcCall) {
    	//pozivi globalnih funckija
    	Obj func = funcCall.getDesignator().obj;
    	if(Obj.Meth == func.getKind()){
    		report_info("Poziv globalne funkcije -> "+ getKind(func.getKind()) +" " +func.getName() + ": "+ getType(func.getType().getKind())
        	+" "+ func.getAdr()+","+func.getLevel()+  " " , funcCall.getParent());
    	}else{
			report_error("Greska na liniji " + funcCall.getParent().getLine()+"  nije funkcija!", null);
    	}
    	
    	
		String funcName = funcCall.getDesignator().getName();
		Obj method = Tab.find(funcName);
		
		
		if(func.getLevel()!= actParams.size()) {
			report_error("Razlicit broj formlanih i stvarnih parametara metode " + funcName, funcCall.getParent());
			return;
		}
		
		if(func.getLevel()==0)
			return;
		
		int i = 0;
		for(Obj o : method.getLocalSymbols()) {
			//if(!o.getType().compatibleWith(actParams.get(i)))
			if(func.getName().equals("len") && actParams.get(i).isRefType()) { actParams.clear(); return; }
			report_info(o.getType().getKind()+"-"+actParams.get(i).getKind(), null);
		    if(!(o.getType().equals(actParams.get(i)) || o.getType() == Tab.nullType && actParams.get(i).getKind()==Struct.Array
				|| actParams.get(i) == Tab.nullType && o.getType().getKind()==Struct.Array))
					report_error("Doslo je do nepoklapanja "+i+"-og stvarnog i formalnog parametra funkcije", funcCall.getParent());
			i++;
			if(i==method.getLevel())
				break;
		}
		actParams.clear();
    	
    }
    
    public void visit(MethodTypeNameNonVoid methodTypeNameNonVoid){
	   if(!Tab.noObj.equals(Tab.find(methodTypeNameNonVoid.getMethName()))){
   		report_error("Metoda "+ methodTypeNameNonVoid.getMethName()+" vec postoji u tabeli simbola", methodTypeNameNonVoid);
   	   }
	   
    	currentMethod = Tab.insert(Obj.Meth, methodTypeNameNonVoid.getMethName(), methodTypeNameNonVoid.getType().struct);
    	currentMethod.setLevel(0);
    	methodTypeNameNonVoid.obj = currentMethod;
    	Tab.openScope();
		report_info("Obradjuje se funkcija " + methodTypeNameNonVoid.getMethName(), methodTypeNameNonVoid);
    }
    
    public void visit(MethodTypeVoid methodTypeVoid){
    	if(!Tab.noObj.equals(Tab.find(methodTypeVoid.getMethName()))){
       		report_error("Metoda "+ methodTypeVoid.getMethName()+" vec postoji u tabeli simbola", methodTypeVoid);
       	}
    	
    	if(methodTypeVoid.getMethName().equals("main"))
    		hasMain = true;
    	
    	currentMethod = Tab.insert(Obj.Meth, methodTypeVoid.getMethName(), Tab.noType);
    	currentMethod.setLevel(0);
    	methodTypeVoid.obj = currentMethod;
    	Tab.openScope();
		report_info("Obradjuje se funkcija " + methodTypeVoid.getMethName(), methodTypeVoid);
    }
    
    public void visit(MethodDeclWithVar methodDeclWithVar){
    	if(!returnFound && currentMethod.getType() != Tab.noType){
    		report_error("return found" + returnFound, null);
			report_error("Semanticka greska na liniji " + methodDeclWithVar.getLine() + ": funkcija " + currentMethod.getName() + " nema return iskaz!", null);
    	}
    	//report_info("Kraj obrade funckije ",methodDeclWithVar);
    	Tab.chainLocalSymbols(currentMethod);
    	Tab.closeScope();
    	returnFound = false;
    	currentMethod = null;
    }
    
    public void visit(MethodDeclWithoutVar methodDeclWithoutVar){
    	if(!returnFound && currentMethod.getType() != Tab.noType){
			report_error("Semanticka greska funkcija " + currentMethod.getName() + " nema return iskaz!", null);
    	}
    	Tab.chainLocalSymbols(currentMethod);
    	Tab.closeScope();
    	returnFound = false;
    	currentMethod = null;
    }
    
    
    public void visit(FormalParamDeclVar formalParamDeclVar) {
    	if(!Tab.noObj.equals(Tab.find(formalParamDeclVar.getFormParamName()))){
       		report_error("Ime "+formalParamDeclVar.getFormParamName() +" vec postoji u tabeli simbola", formalParamDeclVar);
       	}
    	
    	Obj varNode = Tab.insert(Obj.Var, formalParamDeclVar.getFormParamName(),Tab.find(currType).getType());
    	report_info("Pristup formalnom parametru metode "+ currentMethod.getName() +" -> "+ getKind(varNode.getKind()) +" " +varNode.getName() + ": "+ getType(varNode.getType().getKind())
    	+" "+ varNode.getAdr()+","+varNode.getLevel()+  " " , formalParamDeclVar);
    	currentMethod.setLevel(currentMethod.getLevel()+1);
    	//report_info("LEVEL INC" + currentMethod.getLevel() , null);
    }
    
    
    public void visit(FormalParamDeclArray formalParamDeclArray) {
    	if(!Tab.noObj.equals(Tab.find(formalParamDeclArray.getFormParamName()))){
       		report_error("Ime "+formalParamDeclArray.getFormParamName() +" vec postoji u tabeli simbola", formalParamDeclArray);
       	}
    	
    	Obj varNode = Tab.insert(Obj.Var, formalParamDeclArray.getFormParamName(),new Struct(Struct.Array,Tab.find(currType).getType()));
    	report_info("Pristup formalnom parametru metode"+ currentMethod.getName() +" -> "+ getKind(varNode.getKind()) +" " +varNode.getName() + ": "+ getType(varNode.getType().getKind())
    	+" "+ varNode.getAdr()+","+varNode.getLevel()+  " " , formalParamDeclArray);
    	currentMethod.setLevel(currentMethod.getLevel()+1);
    	//report_info("LEVEL INC" + currentMethod.getLevel() , null);
    }
    

    public void visit(VarDeclArr varDeclArr) {
    	if(!Tab.noObj.equals(Tab.find(varDeclArr.getVarName()))){
       		report_error("Ime "+varDeclArr.getVarName() +" vec postoji u tabeli simbola", varDeclArr);
       	}
    	
    	Obj varNode = Tab.insert(Obj.Var, varDeclArr.getVarName(),Tab.find(currType).getType());
    	report_info("Deklarisana lokalna promjenjiva -> "+ getKind(varNode.getKind()) +" " +varNode.getName() + ": "+ getType(varNode.getType().getKind())
    	+" "+ varNode.getAdr()+","+varNode.getLevel()+  " " , varDeclArr);
    }
    
    
    
    public void visit(VarDeclSimple varDeclSimple) {
    	if(!Tab.noObj.equals(Tab.find(varDeclSimple.getVarName()))){
       		report_error("Ime "+ varDeclSimple.getVarName()+" vec postoji u tabeli simbola", varDeclSimple);
       	}
    	
    	Obj varNode = Tab.insert(Obj.Var, varDeclSimple.getVarName(),Tab.find(currType).getType());
    	report_info("Deklarisana lokalna promjenjiva -> "+ getKind(varNode.getKind()) +" " +varNode.getName() + ": "+ getType(varNode.getType().getKind())
    	+" "+ varNode.getAdr()+","+varNode.getLevel()+  " " , varDeclSimple);
    }
    
    public void visit(ReturnExprStmt returnExpr) {
    	returnFound = true;
    	
    	if(currentMethod== null) {
    		report_info("Return naredba mora da bude u okviru metode", returnExpr);
    		return;
    	}
    	
    	Struct currMethType = currentMethod.getType();
    	Struct struct=null;
    	
    	ReturnOption obj= returnExpr.getReturnOption();
    	if(obj instanceof ReturnExpr) {
    		
    		obj= (ReturnExpr) obj;
    		Expr expr= ((ReturnExpr) obj).getExpr();
    		struct= expr.struct;
    		
    		report_info("RETURN :" + getType(struct.getKind()) +" -> "+ getType(currMethType.getKind()), returnExpr);
    		
    		if(currMethType.equals(Tab.noType))
    			report_error("Povratni tip metode "+ currentMethod.getName()+" je void", returnExpr);
    		
    		if(!currMethType.compatibleWith(struct)){
    			report_error("Tip izraza u RETURN "+ getType(struct.getKind()) +" naredbi ne slaze se sa tipom povratne vrednosti "
    					+ "funkcije "+ getType(currMethType.getKind())+" "+ currentMethod.getName(), returnExpr);
        	}
    		
    	}else {
    		if(currMethType!= Tab.noType)
    			report_error("Povratni tip metode"+ currentMethod.getName()+" nije void", returnExpr);
    	}
    	
    	
    }

    public void visit(TermExpr termExpr) {
    	termExpr.struct= termExpr.getExprList().struct;
    }
    
    public void visit(MinusTermExpr minusTermExpr) {
    	minusTermExpr.struct= minusTermExpr.getTerm().struct;
    	if( minusTermExpr.struct != Tab.intType) {
    		report_error("Negativan izraz mora biti tipa int", minusTermExpr);
    	}
    }
    
    
    public void visit(AddExpr addExpr) {
    	Struct s1= addExpr.getExprList().struct;
    	Struct s2= addExpr.getTerm().struct;
    	
    	addExpr.struct= s1;
    	
    	if(!s1.equals(Tab.intType) || !s2.equals(Tab.intType))
    		report_error("Izraz moguce racunati samo za element tipa int", addExpr);
    }
    
    public void visit(NoExprList noExprList) {
    	noExprList.struct = noExprList.getTerm().struct;
    }
    
    public void visit(Term term) {
    	term.struct= term.getTermList().struct;
    }
    
    public void visit(ListTerm listTerm) {
    	Struct s1= listTerm.getTermList().struct;
    	Struct s2= listTerm.getFactor().struct;
    	
    	listTerm.struct=  s1;
    	
    	if(s1==null)
    		report_error("s1 NULL", null);
    	
    	if(!s1.equals(Tab.intType) || !s2.equals(Tab.intType))
    		report_error("Izraz moguce samo za element tipa int ", listTerm);
    }
    
    public void visit(NoTermList noTermList) {
    	noTermList.struct = noTermList.getFactor().struct;
    }
    
    public void visit(Const constFactor) {
    	constFactor.struct= Tab.intType;
    }
    
    public void visit(FactorChar factorChar) {
    	factorChar.struct= Tab.charType;
    }
    
    public void visit(FactorBool factorBool) {
    	factorBool.struct = Tab.boolType;
    }
    
    public void visit(FactorNew factorNew) {
    	factorNew.struct= new Struct(Struct.Array,factorNew.getType().struct);
    	
    	if(!factorNew.getExpr().struct.equals(Tab.intType)) {
    		report_error("Izraz unutra [] mora biti tipa int", factorNew);
    	}
    }
    
    public void visit(FactorDesignator factorDesignator) {
    	Obj obj= Tab.find(((Designator)factorDesignator.getDesignator()).getName());
    	
    	if(obj.getType().getKind() == Struct.Array  && factorDesignator.getDesignator().getDesignatorOptions() instanceof DesignatorOptionArray ) {
    		factorDesignator.struct= obj.getType().getElemType();
    	}else
    	 factorDesignator.struct = obj.getType();
    	
    	if(factorDesignator.getFactorDesignatorOptional() instanceof FuncCall) {
    		String funcName = factorDesignator.getDesignator().getName();
    		Obj func = Tab.find(funcName);
    		
    		if(func.getLevel()!= actParams.size()) {
    			report_error("Razlicit broj formlanih i stvarnih parametara metode " + funcName, factorDesignator);
    			return;
    		}
    		
    		int i = 0;
    		for(Obj o : func.getLocalSymbols()) {
    			if(func.getName().equals("len") && actParams.get(i).isRefType()) { actParams.clear(); return; }
    		    if(!(o.getType().equals(actParams.get(i)) || o.getType() == Tab.nullType && actParams.get(i).isRefType()
    				|| actParams.get(i) == Tab.nullType && o.getType().isRefType()))
    					report_error("Doslo je do nepoklapanja "+i+"-og stvarnog i formalnog parametra funkcije. ", factorDesignator);
    			i++;
    			
    			if(i==func.getLevel())
    				break;
    		}
    		actParams.clear();
    	}
    	
    }
    

    public void visit(FactorExpr factorExpr) {
    	factorExpr.struct= factorExpr.getExpr().struct;
    }
    
    public void visit(DesignatorStatementExpr designatorStatementExpr) {
    	Obj obj = Tab.find(designatorStatementExpr.getDesignator().getName());
    	
    	if(obj.getKind()!=Obj.Var) {
    		report_error("Tip mora biti promjenjiva ili element niza", designatorStatementExpr);
    	}
    	
    	/*if(obj.getKind()== Obj.Var && obj.getType().getKind() == Struct.Array) {
    		if(!(designatorStatementExpr.getDesignator().getDesignatorOptions() instanceof DesignatorOptionArray)) {
    			if(!(designatorStatementExpr.getExpr().struct.getKind()== Struct.Array))
    				report_error("Tip mora biti element niza, a ne niz", designatorStatementExpr);
    		}
    	}*/
    	
    	Struct s1 = Tab.find(designatorStatementExpr.getDesignator().getName()).getType();
        Struct s2 = designatorStatementExpr.getExpr().struct;
        
        /*if(s1.getKind()==Struct.Array)
        	s1= s1.getElemType();
        
        if(s2.getKind()==Struct.Array)
        	s2= s2.getElemType();
        */
        
        if(designatorStatementExpr.getDesignator().getDesignatorOptions() instanceof DesignatorOptionArray) { //elem niza
        	s1=s1.getElemType();
        }
        
        if(!(s2.equals(s1) 
				|| 
				(s2 == Tab.nullType && s1.isRefType())
				|| 
				(s2.getKind() == Struct.Array && s1.getKind() == Struct.Array && s1.getElemType() == Tab.noType))){
			report_error("Tipovi prilikom dodjele vrijednosti nekompatibilani", designatorStatementExpr);
    	}
    }

    public void visit(DesignatorStatementInc designatorStatementInc) {
    	Obj obj = Tab.find(designatorStatementInc.getDesignator().getName());
    	
    	if(obj.getKind()!=Obj.Var) {
    		report_error("Tip mora biti promjenjiva ili element niza", designatorStatementInc);
    	}
    	
    	if(obj.getKind()== Obj.Var && obj.getType().getKind() == Struct.Array) {
    		if(!(designatorStatementInc.getDesignator().getDesignatorOptions() instanceof DesignatorOptionArray)) {
    			report_error("Tip mora biti element niza, a ne niz", designatorStatementInc);
    		}
    	}
    	
    	Struct s = Tab.find(designatorStatementInc.getDesignator().getName()).getType();
    	
    	if(s.getKind()==Struct.Array)
        	s= s.getElemType();
    	
    	if(!s.equals(Tab.intType))
    		report_error("Tipovi prilikom primjene operatora ++ mora biti int", designatorStatementInc);
    }
    
    public void visit(DesignatorStatementDec designatorStatementDec) {
    	Obj obj = Tab.find(designatorStatementDec.getDesignator().getName());
    	
    	if(obj.getKind()!=Obj.Var) {
    		report_error("Tip mora biti promjenjiva ili element niza", designatorStatementDec);
    	}
    	
    	if(obj.getKind()== Obj.Var && obj.getType().getKind() == Struct.Array) {
    		if(!(designatorStatementDec.getDesignator().getDesignatorOptions() instanceof DesignatorOptionArray)) {
    			report_error("Tip mora biti element niza, a ne niz", designatorStatementDec);
    		}
    	}
    	
    	Struct s = Tab.find(designatorStatementDec.getDesignator().getName()).getType();
    	
    	if(s.getKind()==Struct.Array)
        	s= s.getElemType();
    	
    	
    	if(!s.equals(Tab.intType))
    		report_error("Tipovi prilikom primjene operatora -- mora biti int", designatorStatementDec);
    }
    
    public void visit(BreakStatement breakStatement) {
    	if(forDepth < 1)
    		report_error("Break mora da bude unutar for petlje", breakStatement);
    }
    
    public void visit(ContinueStatement continueStatement) {
    	if(forDepth < 1)
    		report_error("Continue mora da bude unutar for petlje", continueStatement);
    }
    
    public void visit(ReadStatement readStatement) {
    	Obj obj = Tab.find(readStatement.getDesignator().getName());
    	
    	if(obj.getKind()!=Obj.Var) {
    		report_error("Read - Tip mora biti promjenjiva ili element niza", readStatement);
    	}
    	
    	if(obj.getKind()== Obj.Var && obj.getType().getKind() == Struct.Array) {
    		if(!(readStatement.getDesignator().getDesignatorOptions() instanceof DesignatorOptionArray)) {
    			report_error("Read - Tip mora biti element niza, a ne niz", readStatement);
    		}
    	}
    	
    	Struct s = Tab.find(readStatement.getDesignator().getName()).getType();
    	
    	if(s.getKind()==Struct.Array)
        	s= s.getElemType();
    	
    	if(!s.equals(Tab.intType) && !s.equals(Tab.charType) && !s.equals(Tab.boolType))
    		report_error("Tipovi prilikom primjene read mora biti int ili char", readStatement);
    	
    	
    }
    
    public void visit(PrintStmt printStmt) {
    	Struct s = printStmt.getExpr().struct;
        
        if(!s.equals(Tab.intType) && !s.equals(Tab.charType) && !s.equals(Tab.boolType)) 
			report_error("Tipovi prilikom primjene print mora biti int, bool ili char", printStmt);
    	
    	
    	printCallCount++;
    } 
    

    public void visit(CondFact condFact) {
    	Struct exprS=  condFact.getExpr().struct;
    	Struct condS= condFact.getCondFactOption().struct;
    	
    	if(condFact.getCondFactOption() instanceof NoCondFactOption) {
    		if(!exprS.equals(Tab.boolType))
    			report_error("Tip izraza pri racunanju uslova mora biti bool", condFact);
    			
    	}else {
    		//tipovi moraju biti kompatibilni
    		//if(!(exprS.compatibleWith(condS)))
    		if(!(condS.equals(exprS) || condS == Tab.nullType && exprS.isRefType()
    				|| exprS == Tab.nullType && condS.isRefType())){
    			report_info("expr:"+exprS.getKind()+" cond:"+condS.getKind(), null);
    			report_error("Tipovi nisu kompatibilni pri primjeni relacionog operatora, pri racunanju uslova", condFact);
    		}
    		//za niz samo == i !=
    		if((exprS.getKind() == Struct.Array) || (condS.getKind() == Struct.Array)) {
    			if(!(( ((CondFactOptionClass)condFact.getCondFactOption()).getRelop() instanceof RelopEquals)
    				|| (((CondFactOptionClass)condFact.getCondFactOption()).getRelop() instanceof RelopNotEquals) ))
    				report_error("Za nizove moguce koriscenje == ili != relacionih operatora", condFact);
    		}
    	}
    	
    	condFact.struct= Tab.boolType;
    }
    
    public void visit(CondFactOptionClass condFactOptionClass) {
    	condFactOptionClass.struct= condFactOptionClass.getExpr().struct;
    }
       
    public void visit(DesignatorOptionArray designatorOptionArray) {
    	if(!designatorOptionArray.getExpr().struct.equals(Tab.intType)) {
    		report_error("Izraz unutra [] mora biti tipa int", designatorOptionArray);
    	}
    }
    
    public void visit(ActualParams actualParams) {
    	Struct s = actualParams.getExpr().struct;
    	actParams.add(s);
    }
    
    public void visit(ActualParam actualParam) {
    	Struct s = actualParam.getExpr().struct;
    	actParams.add(s);
    }
    
    public void visit(NewOp newOp) {
    	if(newOp.getDesignator().obj.getType().getKind()!= Struct.Array &&
    			newOp.getDesignator().obj.getType().getElemType().getKind() != Struct.Int ) {
    		report_error("Desni operator izraza mora biti niz intova", newOp);
    	}
    	
    }
    
    public boolean passed(){
    	if(hasMain == false) {
    		errorDetected=  true;
    		report_error("Ne postoji main metod u programu.", null);
    	}
    	return !errorDetected;
    }
    
   
}
