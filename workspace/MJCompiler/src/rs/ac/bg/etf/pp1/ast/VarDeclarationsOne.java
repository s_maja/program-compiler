// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class VarDeclarationsOne extends VarDeclarationsForClasses {

    private VarDecListType VarDecListType;

    public VarDeclarationsOne (VarDecListType VarDecListType) {
        this.VarDecListType=VarDecListType;
        if(VarDecListType!=null) VarDecListType.setParent(this);
    }

    public VarDecListType getVarDecListType() {
        return VarDecListType;
    }

    public void setVarDecListType(VarDecListType VarDecListType) {
        this.VarDecListType=VarDecListType;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarDecListType!=null) VarDecListType.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarDecListType!=null) VarDecListType.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarDecListType!=null) VarDecListType.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("VarDeclarationsOne(\n");

        if(VarDecListType!=null)
            buffer.append(VarDecListType.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [VarDeclarationsOne]");
        return buffer.toString();
    }
}
