// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class OneVarDeclGlobal extends VarDeclListGlobal {

    private VarDeclGlobal VarDeclGlobal;

    public OneVarDeclGlobal (VarDeclGlobal VarDeclGlobal) {
        this.VarDeclGlobal=VarDeclGlobal;
        if(VarDeclGlobal!=null) VarDeclGlobal.setParent(this);
    }

    public VarDeclGlobal getVarDeclGlobal() {
        return VarDeclGlobal;
    }

    public void setVarDeclGlobal(VarDeclGlobal VarDeclGlobal) {
        this.VarDeclGlobal=VarDeclGlobal;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarDeclGlobal!=null) VarDeclGlobal.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarDeclGlobal!=null) VarDeclGlobal.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarDeclGlobal!=null) VarDeclGlobal.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("OneVarDeclGlobal(\n");

        if(VarDeclGlobal!=null)
            buffer.append(VarDeclGlobal.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [OneVarDeclGlobal]");
        return buffer.toString();
    }
}
