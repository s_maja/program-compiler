// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class UnmatchedIfElse extends Unmatched {

    private DummyStartIf DummyStartIf;
    private ConditionFixed ConditionFixed;
    private Matched Matched;
    private DummyIf DummyIf;
    private Unmatched Unmatched;
    private DummyElse DummyElse;

    public UnmatchedIfElse (DummyStartIf DummyStartIf, ConditionFixed ConditionFixed, Matched Matched, DummyIf DummyIf, Unmatched Unmatched, DummyElse DummyElse) {
        this.DummyStartIf=DummyStartIf;
        if(DummyStartIf!=null) DummyStartIf.setParent(this);
        this.ConditionFixed=ConditionFixed;
        if(ConditionFixed!=null) ConditionFixed.setParent(this);
        this.Matched=Matched;
        if(Matched!=null) Matched.setParent(this);
        this.DummyIf=DummyIf;
        if(DummyIf!=null) DummyIf.setParent(this);
        this.Unmatched=Unmatched;
        if(Unmatched!=null) Unmatched.setParent(this);
        this.DummyElse=DummyElse;
        if(DummyElse!=null) DummyElse.setParent(this);
    }

    public DummyStartIf getDummyStartIf() {
        return DummyStartIf;
    }

    public void setDummyStartIf(DummyStartIf DummyStartIf) {
        this.DummyStartIf=DummyStartIf;
    }

    public ConditionFixed getConditionFixed() {
        return ConditionFixed;
    }

    public void setConditionFixed(ConditionFixed ConditionFixed) {
        this.ConditionFixed=ConditionFixed;
    }

    public Matched getMatched() {
        return Matched;
    }

    public void setMatched(Matched Matched) {
        this.Matched=Matched;
    }

    public DummyIf getDummyIf() {
        return DummyIf;
    }

    public void setDummyIf(DummyIf DummyIf) {
        this.DummyIf=DummyIf;
    }

    public Unmatched getUnmatched() {
        return Unmatched;
    }

    public void setUnmatched(Unmatched Unmatched) {
        this.Unmatched=Unmatched;
    }

    public DummyElse getDummyElse() {
        return DummyElse;
    }

    public void setDummyElse(DummyElse DummyElse) {
        this.DummyElse=DummyElse;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DummyStartIf!=null) DummyStartIf.accept(visitor);
        if(ConditionFixed!=null) ConditionFixed.accept(visitor);
        if(Matched!=null) Matched.accept(visitor);
        if(DummyIf!=null) DummyIf.accept(visitor);
        if(Unmatched!=null) Unmatched.accept(visitor);
        if(DummyElse!=null) DummyElse.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DummyStartIf!=null) DummyStartIf.traverseTopDown(visitor);
        if(ConditionFixed!=null) ConditionFixed.traverseTopDown(visitor);
        if(Matched!=null) Matched.traverseTopDown(visitor);
        if(DummyIf!=null) DummyIf.traverseTopDown(visitor);
        if(Unmatched!=null) Unmatched.traverseTopDown(visitor);
        if(DummyElse!=null) DummyElse.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DummyStartIf!=null) DummyStartIf.traverseBottomUp(visitor);
        if(ConditionFixed!=null) ConditionFixed.traverseBottomUp(visitor);
        if(Matched!=null) Matched.traverseBottomUp(visitor);
        if(DummyIf!=null) DummyIf.traverseBottomUp(visitor);
        if(Unmatched!=null) Unmatched.traverseBottomUp(visitor);
        if(DummyElse!=null) DummyElse.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("UnmatchedIfElse(\n");

        if(DummyStartIf!=null)
            buffer.append(DummyStartIf.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConditionFixed!=null)
            buffer.append(ConditionFixed.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Matched!=null)
            buffer.append(Matched.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DummyIf!=null)
            buffer.append(DummyIf.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Unmatched!=null)
            buffer.append(Unmatched.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DummyElse!=null)
            buffer.append(DummyElse.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [UnmatchedIfElse]");
        return buffer.toString();
    }
}
