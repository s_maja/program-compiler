// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class AbstractClassDeclListTypeExtends extends AbstractClassDeclListType {

    private String AbsClassName;
    private Type Type;
    private AbstractClassDeclList AbstractClassDeclList;

    public AbstractClassDeclListTypeExtends (String AbsClassName, Type Type, AbstractClassDeclList AbstractClassDeclList) {
        this.AbsClassName=AbsClassName;
        this.Type=Type;
        if(Type!=null) Type.setParent(this);
        this.AbstractClassDeclList=AbstractClassDeclList;
        if(AbstractClassDeclList!=null) AbstractClassDeclList.setParent(this);
    }

    public String getAbsClassName() {
        return AbsClassName;
    }

    public void setAbsClassName(String AbsClassName) {
        this.AbsClassName=AbsClassName;
    }

    public Type getType() {
        return Type;
    }

    public void setType(Type Type) {
        this.Type=Type;
    }

    public AbstractClassDeclList getAbstractClassDeclList() {
        return AbstractClassDeclList;
    }

    public void setAbstractClassDeclList(AbstractClassDeclList AbstractClassDeclList) {
        this.AbstractClassDeclList=AbstractClassDeclList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Type!=null) Type.accept(visitor);
        if(AbstractClassDeclList!=null) AbstractClassDeclList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Type!=null) Type.traverseTopDown(visitor);
        if(AbstractClassDeclList!=null) AbstractClassDeclList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Type!=null) Type.traverseBottomUp(visitor);
        if(AbstractClassDeclList!=null) AbstractClassDeclList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractClassDeclListTypeExtends(\n");

        buffer.append(" "+tab+AbsClassName);
        buffer.append("\n");

        if(Type!=null)
            buffer.append(Type.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(AbstractClassDeclList!=null)
            buffer.append(AbstractClassDeclList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractClassDeclListTypeExtends]");
        return buffer.toString();
    }
}
