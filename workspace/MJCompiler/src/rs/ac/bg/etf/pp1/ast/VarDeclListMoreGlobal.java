// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class VarDeclListMoreGlobal extends VarDeclListGlobal {

    private VarDeclListGlobal VarDeclListGlobal;
    private VarDeclGlobal VarDeclGlobal;

    public VarDeclListMoreGlobal (VarDeclListGlobal VarDeclListGlobal, VarDeclGlobal VarDeclGlobal) {
        this.VarDeclListGlobal=VarDeclListGlobal;
        if(VarDeclListGlobal!=null) VarDeclListGlobal.setParent(this);
        this.VarDeclGlobal=VarDeclGlobal;
        if(VarDeclGlobal!=null) VarDeclGlobal.setParent(this);
    }

    public VarDeclListGlobal getVarDeclListGlobal() {
        return VarDeclListGlobal;
    }

    public void setVarDeclListGlobal(VarDeclListGlobal VarDeclListGlobal) {
        this.VarDeclListGlobal=VarDeclListGlobal;
    }

    public VarDeclGlobal getVarDeclGlobal() {
        return VarDeclGlobal;
    }

    public void setVarDeclGlobal(VarDeclGlobal VarDeclGlobal) {
        this.VarDeclGlobal=VarDeclGlobal;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarDeclListGlobal!=null) VarDeclListGlobal.accept(visitor);
        if(VarDeclGlobal!=null) VarDeclGlobal.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarDeclListGlobal!=null) VarDeclListGlobal.traverseTopDown(visitor);
        if(VarDeclGlobal!=null) VarDeclGlobal.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarDeclListGlobal!=null) VarDeclListGlobal.traverseBottomUp(visitor);
        if(VarDeclGlobal!=null) VarDeclGlobal.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("VarDeclListMoreGlobal(\n");

        if(VarDeclListGlobal!=null)
            buffer.append(VarDeclListGlobal.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarDeclGlobal!=null)
            buffer.append(VarDeclGlobal.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [VarDeclListMoreGlobal]");
        return buffer.toString();
    }
}
