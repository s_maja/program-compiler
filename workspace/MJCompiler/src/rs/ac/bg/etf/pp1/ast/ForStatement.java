// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class ForStatement extends Matched {

    private ForStart ForStart;
    private ForDesignatorStmt ForDesignatorStmt;
    private ForCondition ForCondition;
    private DummyJump DummyJump;
    private ForDesignatorStmt ForDesignatorStmt1;
    private DummyForJmpCond DummyForJmpCond;
    private Matched Matched;
    private DummyForJmpInc DummyForJmpInc;

    public ForStatement (ForStart ForStart, ForDesignatorStmt ForDesignatorStmt, ForCondition ForCondition, DummyJump DummyJump, ForDesignatorStmt ForDesignatorStmt1, DummyForJmpCond DummyForJmpCond, Matched Matched, DummyForJmpInc DummyForJmpInc) {
        this.ForStart=ForStart;
        if(ForStart!=null) ForStart.setParent(this);
        this.ForDesignatorStmt=ForDesignatorStmt;
        if(ForDesignatorStmt!=null) ForDesignatorStmt.setParent(this);
        this.ForCondition=ForCondition;
        if(ForCondition!=null) ForCondition.setParent(this);
        this.DummyJump=DummyJump;
        if(DummyJump!=null) DummyJump.setParent(this);
        this.ForDesignatorStmt1=ForDesignatorStmt1;
        if(ForDesignatorStmt1!=null) ForDesignatorStmt1.setParent(this);
        this.DummyForJmpCond=DummyForJmpCond;
        if(DummyForJmpCond!=null) DummyForJmpCond.setParent(this);
        this.Matched=Matched;
        if(Matched!=null) Matched.setParent(this);
        this.DummyForJmpInc=DummyForJmpInc;
        if(DummyForJmpInc!=null) DummyForJmpInc.setParent(this);
    }

    public ForStart getForStart() {
        return ForStart;
    }

    public void setForStart(ForStart ForStart) {
        this.ForStart=ForStart;
    }

    public ForDesignatorStmt getForDesignatorStmt() {
        return ForDesignatorStmt;
    }

    public void setForDesignatorStmt(ForDesignatorStmt ForDesignatorStmt) {
        this.ForDesignatorStmt=ForDesignatorStmt;
    }

    public ForCondition getForCondition() {
        return ForCondition;
    }

    public void setForCondition(ForCondition ForCondition) {
        this.ForCondition=ForCondition;
    }

    public DummyJump getDummyJump() {
        return DummyJump;
    }

    public void setDummyJump(DummyJump DummyJump) {
        this.DummyJump=DummyJump;
    }

    public ForDesignatorStmt getForDesignatorStmt1() {
        return ForDesignatorStmt1;
    }

    public void setForDesignatorStmt1(ForDesignatorStmt ForDesignatorStmt1) {
        this.ForDesignatorStmt1=ForDesignatorStmt1;
    }

    public DummyForJmpCond getDummyForJmpCond() {
        return DummyForJmpCond;
    }

    public void setDummyForJmpCond(DummyForJmpCond DummyForJmpCond) {
        this.DummyForJmpCond=DummyForJmpCond;
    }

    public Matched getMatched() {
        return Matched;
    }

    public void setMatched(Matched Matched) {
        this.Matched=Matched;
    }

    public DummyForJmpInc getDummyForJmpInc() {
        return DummyForJmpInc;
    }

    public void setDummyForJmpInc(DummyForJmpInc DummyForJmpInc) {
        this.DummyForJmpInc=DummyForJmpInc;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ForStart!=null) ForStart.accept(visitor);
        if(ForDesignatorStmt!=null) ForDesignatorStmt.accept(visitor);
        if(ForCondition!=null) ForCondition.accept(visitor);
        if(DummyJump!=null) DummyJump.accept(visitor);
        if(ForDesignatorStmt1!=null) ForDesignatorStmt1.accept(visitor);
        if(DummyForJmpCond!=null) DummyForJmpCond.accept(visitor);
        if(Matched!=null) Matched.accept(visitor);
        if(DummyForJmpInc!=null) DummyForJmpInc.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ForStart!=null) ForStart.traverseTopDown(visitor);
        if(ForDesignatorStmt!=null) ForDesignatorStmt.traverseTopDown(visitor);
        if(ForCondition!=null) ForCondition.traverseTopDown(visitor);
        if(DummyJump!=null) DummyJump.traverseTopDown(visitor);
        if(ForDesignatorStmt1!=null) ForDesignatorStmt1.traverseTopDown(visitor);
        if(DummyForJmpCond!=null) DummyForJmpCond.traverseTopDown(visitor);
        if(Matched!=null) Matched.traverseTopDown(visitor);
        if(DummyForJmpInc!=null) DummyForJmpInc.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ForStart!=null) ForStart.traverseBottomUp(visitor);
        if(ForDesignatorStmt!=null) ForDesignatorStmt.traverseBottomUp(visitor);
        if(ForCondition!=null) ForCondition.traverseBottomUp(visitor);
        if(DummyJump!=null) DummyJump.traverseBottomUp(visitor);
        if(ForDesignatorStmt1!=null) ForDesignatorStmt1.traverseBottomUp(visitor);
        if(DummyForJmpCond!=null) DummyForJmpCond.traverseBottomUp(visitor);
        if(Matched!=null) Matched.traverseBottomUp(visitor);
        if(DummyForJmpInc!=null) DummyForJmpInc.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ForStatement(\n");

        if(ForStart!=null)
            buffer.append(ForStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ForDesignatorStmt!=null)
            buffer.append(ForDesignatorStmt.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ForCondition!=null)
            buffer.append(ForCondition.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DummyJump!=null)
            buffer.append(DummyJump.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ForDesignatorStmt1!=null)
            buffer.append(ForDesignatorStmt1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DummyForJmpCond!=null)
            buffer.append(DummyForJmpCond.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Matched!=null)
            buffer.append(Matched.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DummyForJmpInc!=null)
            buffer.append(DummyForJmpInc.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ForStatement]");
        return buffer.toString();
    }
}
