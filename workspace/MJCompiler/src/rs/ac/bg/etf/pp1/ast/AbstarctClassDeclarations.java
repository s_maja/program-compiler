// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class AbstarctClassDeclarations extends Declarations {

    private Declarations Declarations;
    private AbstractClassDeclListType AbstractClassDeclListType;

    public AbstarctClassDeclarations (Declarations Declarations, AbstractClassDeclListType AbstractClassDeclListType) {
        this.Declarations=Declarations;
        if(Declarations!=null) Declarations.setParent(this);
        this.AbstractClassDeclListType=AbstractClassDeclListType;
        if(AbstractClassDeclListType!=null) AbstractClassDeclListType.setParent(this);
    }

    public Declarations getDeclarations() {
        return Declarations;
    }

    public void setDeclarations(Declarations Declarations) {
        this.Declarations=Declarations;
    }

    public AbstractClassDeclListType getAbstractClassDeclListType() {
        return AbstractClassDeclListType;
    }

    public void setAbstractClassDeclListType(AbstractClassDeclListType AbstractClassDeclListType) {
        this.AbstractClassDeclListType=AbstractClassDeclListType;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Declarations!=null) Declarations.accept(visitor);
        if(AbstractClassDeclListType!=null) AbstractClassDeclListType.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Declarations!=null) Declarations.traverseTopDown(visitor);
        if(AbstractClassDeclListType!=null) AbstractClassDeclListType.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Declarations!=null) Declarations.traverseBottomUp(visitor);
        if(AbstractClassDeclListType!=null) AbstractClassDeclListType.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstarctClassDeclarations(\n");

        if(Declarations!=null)
            buffer.append(Declarations.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(AbstractClassDeclListType!=null)
            buffer.append(AbstractClassDeclListType.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstarctClassDeclarations]");
        return buffer.toString();
    }
}
