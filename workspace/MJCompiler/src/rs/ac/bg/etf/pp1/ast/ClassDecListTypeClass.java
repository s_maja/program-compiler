// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class ClassDecListTypeClass extends ClassDecListType {

    private String className;
    private ClassDecList ClassDecList;

    public ClassDecListTypeClass (String className, ClassDecList ClassDecList) {
        this.className=className;
        this.ClassDecList=ClassDecList;
        if(ClassDecList!=null) ClassDecList.setParent(this);
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className=className;
    }

    public ClassDecList getClassDecList() {
        return ClassDecList;
    }

    public void setClassDecList(ClassDecList ClassDecList) {
        this.ClassDecList=ClassDecList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ClassDecList!=null) ClassDecList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ClassDecList!=null) ClassDecList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ClassDecList!=null) ClassDecList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ClassDecListTypeClass(\n");

        buffer.append(" "+tab+className);
        buffer.append("\n");

        if(ClassDecList!=null)
            buffer.append(ClassDecList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ClassDecListTypeClass]");
        return buffer.toString();
    }
}
