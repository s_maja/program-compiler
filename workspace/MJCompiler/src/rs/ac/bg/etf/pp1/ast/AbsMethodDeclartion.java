// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class AbsMethodDeclartion extends MethodDeclartions {

    private MethodDeclartions MethodDeclartions;
    private AbstractMethodDecl AbstractMethodDecl;

    public AbsMethodDeclartion (MethodDeclartions MethodDeclartions, AbstractMethodDecl AbstractMethodDecl) {
        this.MethodDeclartions=MethodDeclartions;
        if(MethodDeclartions!=null) MethodDeclartions.setParent(this);
        this.AbstractMethodDecl=AbstractMethodDecl;
        if(AbstractMethodDecl!=null) AbstractMethodDecl.setParent(this);
    }

    public MethodDeclartions getMethodDeclartions() {
        return MethodDeclartions;
    }

    public void setMethodDeclartions(MethodDeclartions MethodDeclartions) {
        this.MethodDeclartions=MethodDeclartions;
    }

    public AbstractMethodDecl getAbstractMethodDecl() {
        return AbstractMethodDecl;
    }

    public void setAbstractMethodDecl(AbstractMethodDecl AbstractMethodDecl) {
        this.AbstractMethodDecl=AbstractMethodDecl;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(MethodDeclartions!=null) MethodDeclartions.accept(visitor);
        if(AbstractMethodDecl!=null) AbstractMethodDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(MethodDeclartions!=null) MethodDeclartions.traverseTopDown(visitor);
        if(AbstractMethodDecl!=null) AbstractMethodDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(MethodDeclartions!=null) MethodDeclartions.traverseBottomUp(visitor);
        if(AbstractMethodDecl!=null) AbstractMethodDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbsMethodDeclartion(\n");

        if(MethodDeclartions!=null)
            buffer.append(MethodDeclartions.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(AbstractMethodDecl!=null)
            buffer.append(AbstractMethodDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbsMethodDeclartion]");
        return buffer.toString();
    }
}
