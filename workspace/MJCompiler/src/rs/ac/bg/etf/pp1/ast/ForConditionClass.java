// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class ForConditionClass extends ForCondition {

    private DummyForCondStart DummyForCondStart;
    private ConditionFixed ConditionFixed;

    public ForConditionClass (DummyForCondStart DummyForCondStart, ConditionFixed ConditionFixed) {
        this.DummyForCondStart=DummyForCondStart;
        if(DummyForCondStart!=null) DummyForCondStart.setParent(this);
        this.ConditionFixed=ConditionFixed;
        if(ConditionFixed!=null) ConditionFixed.setParent(this);
    }

    public DummyForCondStart getDummyForCondStart() {
        return DummyForCondStart;
    }

    public void setDummyForCondStart(DummyForCondStart DummyForCondStart) {
        this.DummyForCondStart=DummyForCondStart;
    }

    public ConditionFixed getConditionFixed() {
        return ConditionFixed;
    }

    public void setConditionFixed(ConditionFixed ConditionFixed) {
        this.ConditionFixed=ConditionFixed;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DummyForCondStart!=null) DummyForCondStart.accept(visitor);
        if(ConditionFixed!=null) ConditionFixed.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DummyForCondStart!=null) DummyForCondStart.traverseTopDown(visitor);
        if(ConditionFixed!=null) ConditionFixed.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DummyForCondStart!=null) DummyForCondStart.traverseBottomUp(visitor);
        if(ConditionFixed!=null) ConditionFixed.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ForConditionClass(\n");

        if(DummyForCondStart!=null)
            buffer.append(DummyForCondStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConditionFixed!=null)
            buffer.append(ConditionFixed.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ForConditionClass]");
        return buffer.toString();
    }
}
