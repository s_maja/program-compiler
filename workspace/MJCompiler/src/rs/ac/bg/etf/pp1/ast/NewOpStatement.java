// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class NewOpStatement extends DesignatorStatement {

    private NewOp NewOp;

    public NewOpStatement (NewOp NewOp) {
        this.NewOp=NewOp;
        if(NewOp!=null) NewOp.setParent(this);
    }

    public NewOp getNewOp() {
        return NewOp;
    }

    public void setNewOp(NewOp NewOp) {
        this.NewOp=NewOp;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(NewOp!=null) NewOp.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(NewOp!=null) NewOp.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(NewOp!=null) NewOp.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("NewOpStatement(\n");

        if(NewOp!=null)
            buffer.append(NewOp.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [NewOpStatement]");
        return buffer.toString();
    }
}
