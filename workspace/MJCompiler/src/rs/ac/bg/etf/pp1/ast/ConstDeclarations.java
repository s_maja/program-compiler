// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:27


package rs.ac.bg.etf.pp1.ast;

public class ConstDeclarations extends Declarations {

    private Declarations Declarations;
    private ConstDecListType ConstDecListType;

    public ConstDeclarations (Declarations Declarations, ConstDecListType ConstDecListType) {
        this.Declarations=Declarations;
        if(Declarations!=null) Declarations.setParent(this);
        this.ConstDecListType=ConstDecListType;
        if(ConstDecListType!=null) ConstDecListType.setParent(this);
    }

    public Declarations getDeclarations() {
        return Declarations;
    }

    public void setDeclarations(Declarations Declarations) {
        this.Declarations=Declarations;
    }

    public ConstDecListType getConstDecListType() {
        return ConstDecListType;
    }

    public void setConstDecListType(ConstDecListType ConstDecListType) {
        this.ConstDecListType=ConstDecListType;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Declarations!=null) Declarations.accept(visitor);
        if(ConstDecListType!=null) ConstDecListType.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Declarations!=null) Declarations.traverseTopDown(visitor);
        if(ConstDecListType!=null) ConstDecListType.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Declarations!=null) Declarations.traverseBottomUp(visitor);
        if(ConstDecListType!=null) ConstDecListType.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ConstDeclarations(\n");

        if(Declarations!=null)
            buffer.append(Declarations.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConstDecListType!=null)
            buffer.append(ConstDecListType.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ConstDeclarations]");
        return buffer.toString();
    }
}
