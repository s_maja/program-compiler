// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:27


package rs.ac.bg.etf.pp1.ast;

public class VarDeclarations extends Declarations {

    private Declarations Declarations;
    private VarDecListTypeGlobal VarDecListTypeGlobal;

    public VarDeclarations (Declarations Declarations, VarDecListTypeGlobal VarDecListTypeGlobal) {
        this.Declarations=Declarations;
        if(Declarations!=null) Declarations.setParent(this);
        this.VarDecListTypeGlobal=VarDecListTypeGlobal;
        if(VarDecListTypeGlobal!=null) VarDecListTypeGlobal.setParent(this);
    }

    public Declarations getDeclarations() {
        return Declarations;
    }

    public void setDeclarations(Declarations Declarations) {
        this.Declarations=Declarations;
    }

    public VarDecListTypeGlobal getVarDecListTypeGlobal() {
        return VarDecListTypeGlobal;
    }

    public void setVarDecListTypeGlobal(VarDecListTypeGlobal VarDecListTypeGlobal) {
        this.VarDecListTypeGlobal=VarDecListTypeGlobal;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Declarations!=null) Declarations.accept(visitor);
        if(VarDecListTypeGlobal!=null) VarDecListTypeGlobal.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Declarations!=null) Declarations.traverseTopDown(visitor);
        if(VarDecListTypeGlobal!=null) VarDecListTypeGlobal.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Declarations!=null) Declarations.traverseBottomUp(visitor);
        if(VarDecListTypeGlobal!=null) VarDecListTypeGlobal.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("VarDeclarations(\n");

        if(Declarations!=null)
            buffer.append(Declarations.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarDecListTypeGlobal!=null)
            buffer.append(VarDecListTypeGlobal.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [VarDeclarations]");
        return buffer.toString();
    }
}
