// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class ClassDecListVarMethods extends ClassDecList {

    private VarDeclarationsForClasses VarDeclarationsForClasses;
    private MethodDeclList MethodDeclList;

    public ClassDecListVarMethods (VarDeclarationsForClasses VarDeclarationsForClasses, MethodDeclList MethodDeclList) {
        this.VarDeclarationsForClasses=VarDeclarationsForClasses;
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.setParent(this);
        this.MethodDeclList=MethodDeclList;
        if(MethodDeclList!=null) MethodDeclList.setParent(this);
    }

    public VarDeclarationsForClasses getVarDeclarationsForClasses() {
        return VarDeclarationsForClasses;
    }

    public void setVarDeclarationsForClasses(VarDeclarationsForClasses VarDeclarationsForClasses) {
        this.VarDeclarationsForClasses=VarDeclarationsForClasses;
    }

    public MethodDeclList getMethodDeclList() {
        return MethodDeclList;
    }

    public void setMethodDeclList(MethodDeclList MethodDeclList) {
        this.MethodDeclList=MethodDeclList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.accept(visitor);
        if(MethodDeclList!=null) MethodDeclList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.traverseTopDown(visitor);
        if(MethodDeclList!=null) MethodDeclList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.traverseBottomUp(visitor);
        if(MethodDeclList!=null) MethodDeclList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ClassDecListVarMethods(\n");

        if(VarDeclarationsForClasses!=null)
            buffer.append(VarDeclarationsForClasses.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodDeclList!=null)
            buffer.append(MethodDeclList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ClassDecListVarMethods]");
        return buffer.toString();
    }
}
