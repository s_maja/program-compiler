// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class AbstractClassDeclListVar extends AbstractClassDeclList {

    private VarDeclarationsForClasses VarDeclarationsForClasses;

    public AbstractClassDeclListVar (VarDeclarationsForClasses VarDeclarationsForClasses) {
        this.VarDeclarationsForClasses=VarDeclarationsForClasses;
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.setParent(this);
    }

    public VarDeclarationsForClasses getVarDeclarationsForClasses() {
        return VarDeclarationsForClasses;
    }

    public void setVarDeclarationsForClasses(VarDeclarationsForClasses VarDeclarationsForClasses) {
        this.VarDeclarationsForClasses=VarDeclarationsForClasses;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractClassDeclListVar(\n");

        if(VarDeclarationsForClasses!=null)
            buffer.append(VarDeclarationsForClasses.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractClassDeclListVar]");
        return buffer.toString();
    }
}
