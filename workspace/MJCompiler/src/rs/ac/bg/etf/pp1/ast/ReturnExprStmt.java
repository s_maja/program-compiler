// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class ReturnExprStmt extends Matched {

    private ReturnOption ReturnOption;

    public ReturnExprStmt (ReturnOption ReturnOption) {
        this.ReturnOption=ReturnOption;
        if(ReturnOption!=null) ReturnOption.setParent(this);
    }

    public ReturnOption getReturnOption() {
        return ReturnOption;
    }

    public void setReturnOption(ReturnOption ReturnOption) {
        this.ReturnOption=ReturnOption;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ReturnOption!=null) ReturnOption.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ReturnOption!=null) ReturnOption.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ReturnOption!=null) ReturnOption.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ReturnExprStmt(\n");

        if(ReturnOption!=null)
            buffer.append(ReturnOption.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ReturnExprStmt]");
        return buffer.toString();
    }
}
