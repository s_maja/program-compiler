// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class NoForCondition extends ForCondition {

    private DummyForCondStart DummyForCondStart;

    public NoForCondition (DummyForCondStart DummyForCondStart) {
        this.DummyForCondStart=DummyForCondStart;
        if(DummyForCondStart!=null) DummyForCondStart.setParent(this);
    }

    public DummyForCondStart getDummyForCondStart() {
        return DummyForCondStart;
    }

    public void setDummyForCondStart(DummyForCondStart DummyForCondStart) {
        this.DummyForCondStart=DummyForCondStart;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DummyForCondStart!=null) DummyForCondStart.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DummyForCondStart!=null) DummyForCondStart.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DummyForCondStart!=null) DummyForCondStart.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("NoForCondition(\n");

        if(DummyForCondStart!=null)
            buffer.append(DummyForCondStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [NoForCondition]");
        return buffer.toString();
    }
}
