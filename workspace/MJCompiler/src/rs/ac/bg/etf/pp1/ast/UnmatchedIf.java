// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class UnmatchedIf extends Unmatched {

    private DummyStartIf DummyStartIf;
    private ConditionFixed ConditionFixed;
    private Statement Statement;
    private DummyEnd DummyEnd;

    public UnmatchedIf (DummyStartIf DummyStartIf, ConditionFixed ConditionFixed, Statement Statement, DummyEnd DummyEnd) {
        this.DummyStartIf=DummyStartIf;
        if(DummyStartIf!=null) DummyStartIf.setParent(this);
        this.ConditionFixed=ConditionFixed;
        if(ConditionFixed!=null) ConditionFixed.setParent(this);
        this.Statement=Statement;
        if(Statement!=null) Statement.setParent(this);
        this.DummyEnd=DummyEnd;
        if(DummyEnd!=null) DummyEnd.setParent(this);
    }

    public DummyStartIf getDummyStartIf() {
        return DummyStartIf;
    }

    public void setDummyStartIf(DummyStartIf DummyStartIf) {
        this.DummyStartIf=DummyStartIf;
    }

    public ConditionFixed getConditionFixed() {
        return ConditionFixed;
    }

    public void setConditionFixed(ConditionFixed ConditionFixed) {
        this.ConditionFixed=ConditionFixed;
    }

    public Statement getStatement() {
        return Statement;
    }

    public void setStatement(Statement Statement) {
        this.Statement=Statement;
    }

    public DummyEnd getDummyEnd() {
        return DummyEnd;
    }

    public void setDummyEnd(DummyEnd DummyEnd) {
        this.DummyEnd=DummyEnd;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DummyStartIf!=null) DummyStartIf.accept(visitor);
        if(ConditionFixed!=null) ConditionFixed.accept(visitor);
        if(Statement!=null) Statement.accept(visitor);
        if(DummyEnd!=null) DummyEnd.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DummyStartIf!=null) DummyStartIf.traverseTopDown(visitor);
        if(ConditionFixed!=null) ConditionFixed.traverseTopDown(visitor);
        if(Statement!=null) Statement.traverseTopDown(visitor);
        if(DummyEnd!=null) DummyEnd.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DummyStartIf!=null) DummyStartIf.traverseBottomUp(visitor);
        if(ConditionFixed!=null) ConditionFixed.traverseBottomUp(visitor);
        if(Statement!=null) Statement.traverseBottomUp(visitor);
        if(DummyEnd!=null) DummyEnd.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("UnmatchedIf(\n");

        if(DummyStartIf!=null)
            buffer.append(DummyStartIf.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConditionFixed!=null)
            buffer.append(ConditionFixed.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Statement!=null)
            buffer.append(Statement.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DummyEnd!=null)
            buffer.append(DummyEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [UnmatchedIf]");
        return buffer.toString();
    }
}
