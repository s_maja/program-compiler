// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class VarDeclarationsClass extends VarDeclarationsForClasses {

    private VarDeclarationsForClasses VarDeclarationsForClasses;
    private VarDecListType VarDecListType;

    public VarDeclarationsClass (VarDeclarationsForClasses VarDeclarationsForClasses, VarDecListType VarDecListType) {
        this.VarDeclarationsForClasses=VarDeclarationsForClasses;
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.setParent(this);
        this.VarDecListType=VarDecListType;
        if(VarDecListType!=null) VarDecListType.setParent(this);
    }

    public VarDeclarationsForClasses getVarDeclarationsForClasses() {
        return VarDeclarationsForClasses;
    }

    public void setVarDeclarationsForClasses(VarDeclarationsForClasses VarDeclarationsForClasses) {
        this.VarDeclarationsForClasses=VarDeclarationsForClasses;
    }

    public VarDecListType getVarDecListType() {
        return VarDecListType;
    }

    public void setVarDecListType(VarDecListType VarDecListType) {
        this.VarDecListType=VarDecListType;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.accept(visitor);
        if(VarDecListType!=null) VarDecListType.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.traverseTopDown(visitor);
        if(VarDecListType!=null) VarDecListType.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarDeclarationsForClasses!=null) VarDeclarationsForClasses.traverseBottomUp(visitor);
        if(VarDecListType!=null) VarDecListType.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("VarDeclarationsClass(\n");

        if(VarDeclarationsForClasses!=null)
            buffer.append(VarDeclarationsForClasses.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarDecListType!=null)
            buffer.append(VarDecListType.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [VarDeclarationsClass]");
        return buffer.toString();
    }
}
