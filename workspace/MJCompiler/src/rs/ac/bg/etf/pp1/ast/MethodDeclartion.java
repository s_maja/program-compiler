// generated with ast extension for cup
// version 0.8
// 2/1/2020 23:40:28


package rs.ac.bg.etf.pp1.ast;

public class MethodDeclartion extends MethodDeclartions {

    private MethodDeclartions MethodDeclartions;
    private MethodDecl MethodDecl;

    public MethodDeclartion (MethodDeclartions MethodDeclartions, MethodDecl MethodDecl) {
        this.MethodDeclartions=MethodDeclartions;
        if(MethodDeclartions!=null) MethodDeclartions.setParent(this);
        this.MethodDecl=MethodDecl;
        if(MethodDecl!=null) MethodDecl.setParent(this);
    }

    public MethodDeclartions getMethodDeclartions() {
        return MethodDeclartions;
    }

    public void setMethodDeclartions(MethodDeclartions MethodDeclartions) {
        this.MethodDeclartions=MethodDeclartions;
    }

    public MethodDecl getMethodDecl() {
        return MethodDecl;
    }

    public void setMethodDecl(MethodDecl MethodDecl) {
        this.MethodDecl=MethodDecl;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(MethodDeclartions!=null) MethodDeclartions.accept(visitor);
        if(MethodDecl!=null) MethodDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(MethodDeclartions!=null) MethodDeclartions.traverseTopDown(visitor);
        if(MethodDecl!=null) MethodDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(MethodDeclartions!=null) MethodDeclartions.traverseBottomUp(visitor);
        if(MethodDecl!=null) MethodDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("MethodDeclartion(\n");

        if(MethodDeclartions!=null)
            buffer.append(MethodDeclartions.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodDecl!=null)
            buffer.append(MethodDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [MethodDeclartion]");
        return buffer.toString();
    }
}
