package rs.ac.bg.etf.pp1;


import java.util.ArrayList;
import java.util.Stack;
import java.util.List;


import org.apache.log4j.Logger;

import rs.ac.bg.etf.pp1.ast.AddExpr;
import rs.ac.bg.etf.pp1.ast.Addop;
import rs.ac.bg.etf.pp1.ast.Addope;
import rs.ac.bg.etf.pp1.ast.BreakStatement;
import rs.ac.bg.etf.pp1.ast.CondFact;
import rs.ac.bg.etf.pp1.ast.CondFactOptionClass;
import rs.ac.bg.etf.pp1.ast.CondTerm;
import rs.ac.bg.etf.pp1.ast.CondTermMore;
import rs.ac.bg.etf.pp1.ast.ConditionFixed;
import rs.ac.bg.etf.pp1.ast.ConditionTerm;
import rs.ac.bg.etf.pp1.ast.Const;
import rs.ac.bg.etf.pp1.ast.ContinueStatement;
import rs.ac.bg.etf.pp1.ast.Designator;
import rs.ac.bg.etf.pp1.ast.DesignatorOptionArray;
import rs.ac.bg.etf.pp1.ast.DesignatorStatement;
import rs.ac.bg.etf.pp1.ast.DesignatorStatementActParams;
import rs.ac.bg.etf.pp1.ast.DesignatorStatementDec;
import rs.ac.bg.etf.pp1.ast.DesignatorStatementExpr;
import rs.ac.bg.etf.pp1.ast.DesignatorStatementInc;
import rs.ac.bg.etf.pp1.ast.Divop;
import rs.ac.bg.etf.pp1.ast.Dummy;
import rs.ac.bg.etf.pp1.ast.DummyElse;
import rs.ac.bg.etf.pp1.ast.DummyEnd;
import rs.ac.bg.etf.pp1.ast.DummyForCondStart;
import rs.ac.bg.etf.pp1.ast.DummyForJmpCond;
import rs.ac.bg.etf.pp1.ast.DummyForJmpInc;
import rs.ac.bg.etf.pp1.ast.DummyIf;
import rs.ac.bg.etf.pp1.ast.DummyJump;
import rs.ac.bg.etf.pp1.ast.DummyStartIf;
import rs.ac.bg.etf.pp1.ast.Expr;
import rs.ac.bg.etf.pp1.ast.FactorBool;
import rs.ac.bg.etf.pp1.ast.FactorChar;
import rs.ac.bg.etf.pp1.ast.FactorDesignator;
import rs.ac.bg.etf.pp1.ast.FactorExpr;
import rs.ac.bg.etf.pp1.ast.FactorNew;
import rs.ac.bg.etf.pp1.ast.FuncCall;
import rs.ac.bg.etf.pp1.ast.ListTerm;
import rs.ac.bg.etf.pp1.ast.MethodDeclWithVar;
import rs.ac.bg.etf.pp1.ast.MethodDeclWithoutVar;
import rs.ac.bg.etf.pp1.ast.MethodTypeName;
import rs.ac.bg.etf.pp1.ast.MethodTypeNameNonVoid;
import rs.ac.bg.etf.pp1.ast.MethodTypeVoid;
import rs.ac.bg.etf.pp1.ast.MinusTermExpr;
import rs.ac.bg.etf.pp1.ast.Minusop;
import rs.ac.bg.etf.pp1.ast.Modop;
import rs.ac.bg.etf.pp1.ast.Mulop;
import rs.ac.bg.etf.pp1.ast.Mulope;
import rs.ac.bg.etf.pp1.ast.NewOpStatement;
import rs.ac.bg.etf.pp1.ast.NoActuals;
import rs.ac.bg.etf.pp1.ast.NoDesignatorOption;
import rs.ac.bg.etf.pp1.ast.OneConditionTerm;
import rs.ac.bg.etf.pp1.ast.OrTerm;
import rs.ac.bg.etf.pp1.ast.PrintOptionNum;
import rs.ac.bg.etf.pp1.ast.PrintStmt;
import rs.ac.bg.etf.pp1.ast.ReadStatement;
import rs.ac.bg.etf.pp1.ast.RelopEquals;
import rs.ac.bg.etf.pp1.ast.RelopGreater;
import rs.ac.bg.etf.pp1.ast.RelopGreaterEquals;
import rs.ac.bg.etf.pp1.ast.RelopLess;
import rs.ac.bg.etf.pp1.ast.RelopLessEquals;
import rs.ac.bg.etf.pp1.ast.RelopNotEquals;
import rs.ac.bg.etf.pp1.ast.ReturnExprStmt;
import rs.ac.bg.etf.pp1.ast.SyntaxNode;
import rs.ac.bg.etf.pp1.ast.Var;
import rs.ac.bg.etf.pp1.ast.VisitorAdaptor;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class CodeGenerator extends VisitorAdaptor {
	
	Logger log = Logger.getLogger(getClass());
	
	private int mainPc;
	Stack<Stack<Integer>> fixUpOr = new Stack<>();
	Stack<Stack<Integer>> fixUpAnd = new Stack<>();
	Stack<Integer> fixUpAdr = new Stack<>();
	Stack<Integer> fixUpAdrCond = new Stack<>();
	Stack<Integer> fixUpAdrInc = new Stack<>();
	Stack<Integer> fixUpAdrStatement = new Stack<>();
	Stack<Stack<Integer>> breakAdr= new Stack<>();
	int continueAdr =-1;
	int indexArray=-1;
	
	
	
	public CodeGenerator() {
		super();
		initializePredeclaredMethods();
	}
	
	private void initializePredeclaredMethods() {
        // 'ord' and 'chr' are the same code.
        Obj ordMethod = Tab.find("ord");
        Obj chrMethod = Tab.find("chr");
        ordMethod.setAdr(Code.pc);
        chrMethod.setAdr(Code.pc);
        Code.put(Code.enter);
        Code.put(1);
        Code.put(1);
        Code.put(Code.load_n);
        Code.put(Code.exit);
        Code.put(Code.return_);
 
        Obj lenMethod = Tab.find("len");
        lenMethod.setAdr(Code.pc);
        Code.put(Code.enter);
        Code.put(1);
        Code.put(1);
        Code.put(Code.load_n);
        Code.put(Code.arraylength);
        Code.put(Code.exit);
        Code.put(Code.return_);
 
        Obj newOpMeth= Tab.find("newOpMeth");
        newOpMeth.setAdr(Code.pc);
        Code.put(Code.enter);
        Code.put(2);
        Code.put(4);
        
        //for
        Code.put(Code.load_n+0);
        Code.put(Code.call); //len
        Code.put(Code.pc-16);
        Code.put(Code.const_1);
        Code.put(Code.sub);
        Code.put(Code.store_2);
       
        //for cond
        Code.put(Code.load_2);
        Code.put(Code.const_n+0);
        Code.putFalseJump(Code.gt, 86);
        Code.putJump(43);
        
        //for inc
        Code.put(Code.load_2);
        Code.put(Code.const_1);
        Code.put(Code.sub);
        Code.put(Code.store_2);
        Code.putJump(28);
        
        //if-1
        Code.put(Code.load_n+0);
        Code.put(Code.load_2);
        Code.put(Code.aload);
        Code.put(Code.load_1);
        Code.putFalseJump(Code.gt, 80);
        
        //if-2
        Code.put(Code.load_2);
        Code.put(Code.load_n+0);
        Code.put(Code.call);
        Code.put(Code.pc-46);
        Code.put(Code.const_1);
        Code.put(Code.sub);
        Code.putFalseJump(Code.eq, 67);
        
        //niz[i]= num;
        Code.put(Code.load_n+0);
        Code.put(Code.load_2);
        Code.put(Code.load_n+1);
        Code.put(Code.astore);
        Code.putJump(77);
        
        //swap
        Code.put(Code.load_n+0);
        Code.put(Code.load_2);
        Code.put(Code.aload);
        Code.put(Code.store_3);
        Code.put(Code.load_n+0);
        Code.put(Code.load_2);
        Code.put(Code.load_n+0);
        Code.put(Code.load_2);
        Code.put(Code.const_1);
        Code.put(Code.add);
        Code.put(Code.aload);
        Code.put(Code.astore);
        Code.put(Code.load_n+0);
        Code.put(Code.load_2);
        Code.put(Code.const_1);
        Code.put(Code.add);   
        Code.put(Code.load_3);
        Code.put(Code.astore); 
        Code.putJump(83);
        Code.putJump(86);
        Code.putJump(36);
        		
        Code.put(Code.exit);
        Code.put(Code.return_);
    }

	public int getMainPc() {
		return mainPc;
		
	}
	
	public void visit(NewOpStatement newOpStatement) {
		Code.put(Code.call);
		Code.put2(Tab.find("newOpMeth").getAdr()-Code.pc);
		
		Obj niz= newOpStatement.getNewOp().getDesignator().obj;
		int i= newOpStatement.getNewOp().getN2();
		
		
	}

	public void visit(Designator designator) {
		if(designator.getDesignatorOptions() instanceof DesignatorOptionArray) {
			
			if(!(designator.getParent() instanceof DesignatorStatementExpr)) {
				Obj array = designator.obj;
				
				//log.info("Array :"+ array.getKind() + " kind: " +array.getType().getKind()+ " elem: "+ array.getType().getElemType().getKind());
				
				if(!(designator.getParent() instanceof ReadStatement)) {
					
					Obj elem =  new Obj(Obj.Elem, "$$", array.getType().getElemType());
					if(designator.getParent() instanceof DesignatorStatementInc || designator.getParent() instanceof DesignatorStatementDec) {
						Code.put(Code.dup2);
					}
					Code.load(elem);
					//log.info("Usao da loaduje elem " + elem.getKind());
				}
			}
		}
		
		if(designator.obj.getKind()==Obj.Meth)
			return;
		
		if(designator.getDesignatorOptions() instanceof NoDesignatorOption) {
			if(!(designator.getParent() instanceof DesignatorStatement)) {
				//log.info("usao za " + designator.obj.getName());
				Code.load(designator.obj);
			}
		}
	}
	
	public void visit(Dummy dummy) {
		//push array na stek
		String name= ((Designator)((DesignatorOptionArray)dummy.getParent()).getParent()).getName();
		Obj arr = ((Designator)((DesignatorOptionArray)dummy.getParent()).getParent()).obj;
		
		Code.load(arr);
		////log.info("Arr stavljen na stek dummy: "+ name);
	}
	
	public void visit(Const cnst) {
		Obj con = Tab.insert(Obj.Con, "$", cnst.struct);
		con.setLevel(0);
		con.setAdr(cnst.getN1());
		
		Code.load(con);
		
		indexArray = cnst.getN1();
		////log.info("const "+ cnst.getN1());
	}

	public void visit(FactorChar c) {
		Obj con = Tab.insert(Obj.Con, "$", c.struct);
		con.setLevel(0);
		con.setAdr(c.getC1());
		
		Code.load(con);
		
		////log.info("char");
	}

	public void visit(FactorBool bool) {
		Obj con = Tab.insert(Obj.Con, "$", bool.struct);
		con.setLevel(0);
		con.setAdr(bool.getB1() ? 1 : 0);
		
		Code.load(con);
		
		////log.info("bool");
	}
	
	public void visit(FactorNew factorNew) {
		Struct s = factorNew.struct;
		
		
		
		if(s.getElemType().equals(Tab.charType)) {
			Code.put(Code.newarray);
			Code.put(0);
		}else if(s.getElemType().equals(Tab.boolType) || s.getElemType().equals(Tab.intType)) {
			Code.put(Code.newarray);
			Code.put(1);
		}
		
		////log.info("factor new "+s.getKind()+ " tip niza");
	}
	
	public void visit(FactorDesignator funcCall) {
		if(funcCall.getFactorDesignatorOptional() instanceof FuncCall) {
			
			//if(((FuncCall)funcCall.getFactorDesignatorOptional()).getActualPars() instanceof NoActuals)
			Obj method = funcCall.getDesignator().obj;
			
		
			int offset = method.getAdr() - Code.pc;
			Code.put(Code.call);
			Code.put2(offset);
			
			////log.info("funcCall factor des");
		}else {
			
		}
		
		
	}

	public void visit(DesignatorStatementActParams funcCall) {
		//if(funcCall.getActualPars() instanceof NoActuals) {
			Obj method = funcCall.getDesignator().obj;
			
			int offset = method.getAdr() - Code.pc;
			
			Code.put(Code.call);
			Code.put2(offset);
			
			////log.info("func call desig statement");
			
			if(funcCall.getDesignator().obj.getType() != Tab.noType){ //ako nije void 
				Code.put(Code.pop);
			}
		//}
	}
	
	public void visit(MethodTypeNameNonVoid method) {
		String name= method.getMethName();
		
		if("main".equals(name)){
			mainPc = Code.pc;
		}
		
		Obj func = method.obj;
	
		func.setAdr(Code.pc);
		
		// Generate the entry
		Code.put(Code.enter);
		Code.put(func.getLevel());
		Code.put(func.getLocalSymbols().size());
		
		////log.info("method enter");
	}
	
	public void visit(MethodTypeVoid method) {
		String name= method.getMethName();
		
		if("main".equals(name)){
			mainPc = Code.pc;
		}
		
		Obj func = method.obj;
		
		func.setAdr(Code.pc);
		
		// Generate the entry
		Code.put(Code.enter);
		Code.put(func.getLevel());
		Code.put(func.getLocalSymbols().size());
		
		//log.info("method enter");
	}
	
	public void visit(MethodDeclWithVar methodDecl) {
		Code.put(Code.exit);
		Code.put(Code.return_);
		
		//log.info("method exit");
	}
	
	public void visit(MethodDeclWithoutVar methodDecl) {
		Code.put(Code.exit);
		Code.put(Code.return_);
		//log.info("method exit");
	}
	
	public void visit(ListTerm mulExpr) {
		//log.info(mulExpr.getMulop().getClass());
		
		if(mulExpr.getMulop() instanceof Mulope)
			Code.put(Code.mul);
		if(mulExpr.getMulop() instanceof Divop)
			Code.put(Code.div);
		if(mulExpr.getMulop() instanceof Modop)
			Code.put(Code.rem);
		
		//log.info("mul");
	}
	
	public void visit(AddExpr addExpr) {
		if(addExpr.getAddop() instanceof Addope)
			Code.put(Code.add);
		if(addExpr.getAddop() instanceof Minusop)
			Code.put(Code.sub);
	}
	
	public void visit(MinusTermExpr minusTermExpr) {
		Code.put(Code.neg);
	}

	public void visit(PrintStmt printStmt) {
		if(printStmt.getPrintOption() instanceof PrintOptionNum) {
			int n=((PrintOptionNum)printStmt.getPrintOption()).getN1();
			Code.loadConst(n);
		}else {
			Code.loadConst(5); //default
		}
		
		if(printStmt.getExpr().struct.equals(Tab.intType) || printStmt.getExpr().struct.equals(Tab.boolType)){
			Code.put(Code.print);
		}else{
			Code.put(Code.bprint);
		}
		
		//log.info("print");
	}
	
	public void visit(ReadStatement readStatement) {
		Obj obj = readStatement.getDesignator().obj;
		
		Struct s= obj.getType();
		if(s.getKind()== Struct.Array) {
			s= s.getElemType();
		
			obj = new Obj(Obj.Elem, "$$$", s);
		}

		if(s.equals(Tab.intType) || s.equals(Tab.boolType)){
			Code.put(Code.read);
		}else if (s.equals(Tab.charType)){
			Code.put(Code.bread);
		}
		
		//ako je objekat niz moram na steku da imam ...arr,index, val 
		// i da pozovemo za elem
		Code.store(obj);
		
	}
	
	public void visit(DesignatorStatementInc designatorStatementInc) {
		Obj obj = designatorStatementInc.getDesignator().obj;
		
		if(obj.getType().getKind() != Struct.Array) {
			Code.load(obj);
		}
		Code.loadConst(1);
		Code.put(Code.add);
		if(obj.getType().getKind() == Struct.Array) {
			Code.store(new Obj(Obj.Elem, "$",obj.getType().getElemType()));
		}else
		Code.store(obj);
	}
	
	public void visit(DesignatorStatementDec designatorStatementDec) {
		Obj obj = designatorStatementDec.getDesignator().obj;
		if(obj.getType().getKind() != Struct.Array) {
			Code.load(obj);
		}
		Code.loadConst(1);
		Code.put(Code.sub);
		if(obj.getType().getKind() == Struct.Array) {
			Code.store(new Obj(Obj.Elem, "$",obj.getType().getElemType()));
		}else
		Code.store(obj);

	}
	
	public void visit(DesignatorStatementExpr designatorStatementExpr) {
		Obj obj= designatorStatementExpr.getDesignator().obj;
		
		if(designatorStatementExpr.getDesignator().getDesignatorOptions() instanceof DesignatorOptionArray) {
			Obj elem =  new Obj(Obj.Elem, "$$$", obj.getType().getElemType());
			Code.store(elem);
		}else
			Code.store(obj);
		//log.info("usao u assigment i dodjelio "+ obj.getName()+" tipa "+ obj.getType().getKind());
	}
	
	public void visit(ReturnExprStmt ret) {
		Code.put(Code.exit);
		Code.put(Code.return_);
		
		//log.info("return");
	}

	public void visit(CondFact condFact) {
		if(condFact.getCondFactOption() instanceof CondFactOptionClass) {
			int op = Code.eq;
			if(((CondFactOptionClass)condFact.getCondFactOption()).getRelop() instanceof RelopEquals) op= Code.eq;
			if(((CondFactOptionClass)condFact.getCondFactOption()).getRelop() instanceof RelopNotEquals) op= Code.ne;
			if(((CondFactOptionClass)condFact.getCondFactOption()).getRelop() instanceof RelopLess) op= Code.lt;
			if(((CondFactOptionClass)condFact.getCondFactOption()).getRelop() instanceof RelopLessEquals) op= Code.le;
			if(((CondFactOptionClass)condFact.getCondFactOption()).getRelop() instanceof RelopGreater) op= Code.gt;
			if(((CondFactOptionClass)condFact.getCondFactOption()).getRelop() instanceof RelopGreaterEquals) op= Code.ge;
		
			Code.putFalseJump(op, 0);
			fixUpAnd.peek().add(Code.pc-2);
			
			//log.info("Prazan jmp za kod op " + op);
		}else {
			Code.loadConst(0); //poredimo sa 0
			Code.putFalseJump(Code.ne, 0);
			fixUpAnd.peek().add(Code.pc-2);
			
			//log.info("Prazan jmp za samo bool ");
		}
	}
	
	public void visit(OrTerm or) {
		Code.putJump(0); // dummy
	  	fixUpOr.peek().push(Code.pc - 2);
	  	
	  	while(!fixUpAnd.peek().empty())
	  		Code.fixup(fixUpAnd.peek().pop());
	  	
	  	//log.info("Or");
	}
	
	public void visit(ConditionFixed conditionFixed) {
		while(!fixUpOr.peek().empty()) {
	  		Code.fixup(fixUpOr.peek().pop());
	  		//log.info("pop");
		}
		//log.info("condition");
	}
	
	public void visit(DummyEnd dummy) {
		while(!fixUpAnd.peek().empty())
	  		Code.fixup(fixUpAnd.peek().pop());
		
		fixUpAnd.pop();
		fixUpOr.pop();
		
		//log.info("dummmy end");
	}

	public void visit(DummyElse dummy) {
		Code.fixup(fixUpAdr.pop());
		
		fixUpAnd.pop();
		fixUpOr.pop();
		
		//log.info("dummy else");
	}
	
	public void visit(DummyIf dummy) {
		Code.putJump(0);
		fixUpAdr.push(Code.pc-2);
		
		while(!fixUpAnd.peek().empty()) {
	  		Code.fixup(fixUpAnd.peek().pop());
	  		//log.info("pop and");
		}
		
		//log.info("dummy if");
		
	}
	
	public void visit(DummyStartIf dummy) {
		fixUpAnd.push(new Stack<>());
		fixUpOr.push(new Stack<>());
		
		//log.info("kreiraj stek");
	}

	public void visit(DummyForCondStart dummy) {
		fixUpAnd.push(new Stack<>());
		fixUpOr.push(new Stack<>());
		breakAdr.push(new Stack<>());
		
		fixUpAdrCond.add(Code.pc); //cuvam adresu pocetka forUslova
		
		//log.info("kreiraj stek for");
	}
	
	public void visit(DummyForJmpCond d) {
		//posle inc skoci na ispitivanje uslova -- unazad
		Code.putJump(fixUpAdrCond.pop());
		
		Code.fixup(fixUpAdrStatement.pop()); //fixUp adrese da se preskoci inc posle uslova
		
		//log.info("jmp na uslov");
	}
	
	public void visit(DummyForJmpInc d) {
		//kad se stigne do kraja for skoci na inc statement -- unazad
		Code.putJump(fixUpAdrInc.pop());
		
		//log.info("jmp na inc");
		
		while(!fixUpAnd.peek().empty())
	  		Code.fixup(fixUpAnd.peek().pop());
		
		while(!breakAdr.peek().empty()) {
			Code.fixup(breakAdr.peek().pop());
		}
		
		fixUpAnd.pop();
		fixUpOr.pop();
		breakAdr.pop();
		
		//log.info("dummmy end");
		
		
		
	}
	
	public void visit(DummyJump d) {		
		//bezuslovni skok da se preskoci inc kad treba statement da se izvrsava -- unaprijed
		Code.putJump(0);
		fixUpAdrStatement.add(Code.pc -2);
		
				
		fixUpAdrInc.add(Code.pc); //sacuvam adresu inc
		
		//log.info("skoci na statemente");
	}
	

	public void visit(BreakStatement breakStm) {
		Code.putJump(0);
		breakAdr.peek().push(Code.pc-2);
	}
	
	public void visit(ContinueStatement con) {
		Code.putJump(fixUpAdrInc.peek());
	}
}

